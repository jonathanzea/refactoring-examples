package com.zea.refactors.movingbehaviourintotheclass.before

class Product(val name: String, val amount: Double)
