package com.zea.refactors.movingbehaviourintotheclass.before

class BankCustomer {

    private val products = mutableListOf<Product>()

    fun addProduct(product: Product) = this.products.add(product)
    fun getProducts(): List<Product> = products
}
