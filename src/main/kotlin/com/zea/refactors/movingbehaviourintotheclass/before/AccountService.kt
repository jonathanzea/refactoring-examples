package com.zea.refactors.movingbehaviourintotheclass.before

class AccountService(val customer: BankCustomer) {

    fun customerTotalProducts() = customer.getProducts().size
    fun customerTotalLoansAmount() = customer.getProducts().filter { it.name == "LOAN" }.map { it.amount }.sum()
}
