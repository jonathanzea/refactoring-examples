package com.zea.refactors.movingbehaviourintotheclass.after

class AccountService(val customer: BankCustomer) {

    fun customerTotalProducts() = customer.totalProducts()
    fun customerTotalLoansAmount() = customer.customerTotalLoansAmount()
}
