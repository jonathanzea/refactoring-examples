package com.zea.refactors.movingbehaviourintotheclass.after

class Product(val name: String, val amount: Double)
