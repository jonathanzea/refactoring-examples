package com.zea.refactors.movingbehaviourintotheclass.after

class BankCustomer {

    private val products = mutableListOf<Product>()

    fun addProduct(product: Product) = this.products.add(product)
    fun totalProducts() = this.products.size
    fun customerTotalLoansAmount() = this.products.filter { it.name == "LOAN" }.map { it.amount }.sum()
}
