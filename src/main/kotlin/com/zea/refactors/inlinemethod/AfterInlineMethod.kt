package com.zea.refactors.inlinemethod

class AfterInlineMethod(private val lateDeliveries: Int) {
    fun rating(): Int = if (lateDeliveries > 5) 2 else 1
}
