package com.zea.refactors.inlinemethod

class AfterInlineMethodWithTemp(private val lateDeliveries: Int) {
    fun rating(): Int = if (lateDeliveries > 5 * factor()) 2 else 1
    private fun factor(): Double = 0.5
}
