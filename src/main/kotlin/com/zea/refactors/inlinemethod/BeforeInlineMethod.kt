package com.zea.refactors.inlinemethod

class BeforeInlineMethod(private val lateDeliveries: Int) {

    fun rating(): Int = if (moreThanFiveLateDeliveries()) 2 else 1
    private fun moreThanFiveLateDeliveries() = lateDeliveries > 5
}
