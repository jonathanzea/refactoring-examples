package com.zea.refactors.splittemporaryvariable

class BeforeSplitTemporaryVariable(var asAbsolute: Boolean, var file: String) {

    fun createPath(): String {
        var path = "users/jonathan/bin/"
        if (asAbsolute) {
            path = "/usr/bin/"
        }
        return "$path$file"
    }
}
