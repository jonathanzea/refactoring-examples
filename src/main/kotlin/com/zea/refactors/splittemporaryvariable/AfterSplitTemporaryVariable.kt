package com.zea.refactors.splittemporaryvariable

class AfterSplitTemporaryVariable(private val asAbsolute: Boolean, private val file: String) {

    fun createPath(): String {
        val relativePath = "users/jonathan/bin/"
        if (asAbsolute) {
            val absolutePath = "/usr/bin/"
            return "$absolutePath$file"
        }
        return "$relativePath$file"
    }
}
