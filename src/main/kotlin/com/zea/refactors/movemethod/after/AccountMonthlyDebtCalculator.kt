package com.zea.refactors.movemethod.after

class AccountMonthlyDebtCalculator(private val accountType: AccountType, private val daysOverdrawn: Int) {

    fun calculate(): Double {
        var result = 4.5
        if (daysOverdrawn > 0) result += accountType.overdraftCharge(daysOverdrawn)
        return result
    }
}
