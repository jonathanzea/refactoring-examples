package com.zea.refactors.movemethod.after

class AccountType(private val typeIdentifier: String) {

    fun overdraftCharge(daysOverdrawn: Int): Double {
        return if (typeIdentifier == "PREMIUM") {
            var result = 8.0
            if (daysOverdrawn > 7) {
                result += (daysOverdrawn) * 0.85
            }
            result
        } else {
            daysOverdrawn * 1.75
        }
    }
}
