package com.zea.refactors.movemethod.before

class AccountMonthlyDebtCalculator(val accountType: AccountType, val daysOverdrawn: Int) {

    fun calculate(): Double {
        var result = 4.5
        if (daysOverdrawn > 0) result += overdraftCharge()
        return result
    }

    private fun overdraftCharge(): Double {
        return if (accountType.isPremium()) {
            var result = 8.0
            if (daysOverdrawn > 7) {
                result += (daysOverdrawn) * 0.85
            }
            result
        } else {
            daysOverdrawn * 1.75
        }
    }
}
