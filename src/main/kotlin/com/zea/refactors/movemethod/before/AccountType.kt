package com.zea.refactors.movemethod.before

class AccountType(val typeIdentifier: String) {
    fun isPremium() = typeIdentifier == "PREMIUM"
}
