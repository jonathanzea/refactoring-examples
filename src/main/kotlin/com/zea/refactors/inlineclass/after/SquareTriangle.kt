package com.zea.refactors.inlineclass.after

import kotlin.math.pow
import kotlin.math.sqrt

class SquareTriangle(private val adjacent: Double, private val opposite: Double) {
    fun calculateHypotenuseByPythagoras() = sqrt(adjacent.pow(2.0) + opposite.pow(2.0))
}
