package com.zea.refactors.inlineclass.before

class SquareTriangle(private val adjacent: Double, private val opposite: Double, private val pythagoras: Pythagoras) {
    fun getHypotenuse() = pythagoras.calculateHypotenuse(adjacent, opposite)
}
