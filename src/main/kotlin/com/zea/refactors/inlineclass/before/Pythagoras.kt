package com.zea.refactors.inlineclass.before

import kotlin.math.pow
import kotlin.math.sqrt

class Pythagoras {

    fun calculateHypotenuse(adjacent: Double, opposite: Double) =
        sqrt(adjacent.pow(2.0) + opposite.pow(2.0))
}
