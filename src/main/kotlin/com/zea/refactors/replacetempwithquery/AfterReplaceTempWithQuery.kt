package com.zea.refactors.replacetempwithquery

class AfterReplaceTempWithQuery(var quantity: Int, var price: Double) {

    fun calculatePrice(): Double {
        return basePrice() * discountFactor()
    }

    private fun discountFactor() = if (basePrice() > 1000.0) .95 else .98

    private fun basePrice() = quantity * price
}
