package com.zea.refactors.replacetempwithquery

class BeforeReplaceTempWithQuery(var quantity: Int, var price: Double) {

    fun calculatePrice(): Double {
        var basePrice = quantity * price
        var discountFactor: Double
        discountFactor = if (basePrice > 1000.0) .95 else .98
        return basePrice * discountFactor
    }
}
