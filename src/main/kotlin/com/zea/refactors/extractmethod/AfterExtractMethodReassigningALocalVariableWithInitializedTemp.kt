package com.zea.refactors.extractmethod

// EXTRACT METHOD - 3.1. Reassigning a Local Variables - When the temp `totalDebt` comes with a previous value
class AfterExtractMethodReassigningALocalVariableWithInitializedTemp(val name: String, val housingDebts: List<Double>) {

    fun printDebt(carDebt: Double) {
        printBanner()
        val totalDebt = getTotalDebt(carDebt * 1.5)
        printDetails(totalDebt)
    }

    private fun printBanner() {
        println("************************")
        println("DEBT CALCULATOR")
        println("************************")
    }

    private fun getTotalDebt(initialDebt: Double): Double {
        var totalDebt = initialDebt
        housingDebts.forEach { totalDebt += it }
        return totalDebt
    }

    private fun printDetails(totalDebt: Double) {
        println("Name: $name")
        println("Total debt: $$totalDebt")
    }
}
