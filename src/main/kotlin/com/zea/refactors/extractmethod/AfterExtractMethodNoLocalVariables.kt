package com.zea.refactors.extractmethod

// EXTRACT METHOD - 1. Using No Local Variables
class AfterExtractMethodNoLocalVariables(val name: String, val debts: List<Double>) {

    fun printDebt() {
        var totalDebt = 0.0
        printBanner()
        debts.forEach { totalDebt += it }
        println("Name: $name")
        println("Total debt: $$totalDebt")
    }

    private fun printBanner() {
        println("************************")
        println("DEBT CALCULATOR")
        println("************************")
    }
}
