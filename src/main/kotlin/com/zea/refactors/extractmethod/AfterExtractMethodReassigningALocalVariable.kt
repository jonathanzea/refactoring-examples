package com.zea.refactors.extractmethod

// EXTRACT METHOD - 4. Reassigning a Local Variables
class AfterExtractMethodReassigningALocalVariable(val name: String, val debts: List<Double>) {

    fun printDebt() {
        printBanner()
        val totalDebt = getTotalDebt()
        printDetails(totalDebt)
    }

    private fun printBanner() {
        println("************************")
        println("DEBT CALCULATOR")
        println("************************")
    }

    private fun getTotalDebt(): Double {
        var totalDebt = 0.0
        debts.forEach { totalDebt += it }
        return totalDebt
    }

    private fun printDetails(totalDebt: Double) {
        println("Name: $name")
        println("Total debt: $$totalDebt")
    }
}
