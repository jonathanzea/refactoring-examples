package com.zea.refactors.extractmethod

// EXTRACT METHOD - 0. Original case
class BeforeExtractMethod(val name: String, val debts: List<Double>) {

    fun printDebt() {
        var totalDebt = 0.0

        println("************************")
        println("DEBT CALCULATOR")
        println("************************")

        debts.forEach { totalDebt += it }

        println("Name: $name")
        println("Total debt: $$totalDebt")
    }
}
