package com.zea.refactors.extractmethod

// EXTRACT METHOD - 2. Using Local Variables
class AfterExtractMethodUsingLocalVariables(val name: String, val debts: List<Double>) {

    fun printDebt() {
        var totalDebt = 0.0
        printBanner()
        debts.forEach { totalDebt += it }
        printDetails(totalDebt)
    }

    private fun printBanner() {
        println("************************")
        println("DEBT CALCULATOR")
        println("************************")
    }

    private fun printDetails(totalDebt: Double) {
        println("Name: $name")
        println("Total debt: $$totalDebt")
    }
}
