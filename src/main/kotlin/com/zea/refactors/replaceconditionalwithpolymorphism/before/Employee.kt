package com.zea.refactors.replaceconditionalwithpolymorphism.before

class Employee(private val type: EmployeeType) {

    fun getSalary(): Double =
        when (type) {
            EmployeeType.ENGINEER -> 50000.0
            EmployeeType.SALESMAN -> 20000.0
            EmployeeType.MANAGER -> 80000.0
        }
}
