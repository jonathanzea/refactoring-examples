package com.zea.refactors.replaceconditionalwithpolymorphism.before

enum class EmployeeType {
    ENGINEER,
    SALESMAN,
    MANAGER
}
