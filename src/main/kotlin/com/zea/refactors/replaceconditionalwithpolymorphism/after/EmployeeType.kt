package com.zea.refactors.replaceconditionalwithpolymorphism.after

interface EmployeeType {
    fun calculateSalary(): Double
}
