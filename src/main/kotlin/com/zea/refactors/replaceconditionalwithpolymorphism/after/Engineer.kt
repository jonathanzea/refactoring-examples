package com.zea.refactors.replaceconditionalwithpolymorphism.after

class Engineer : EmployeeType {
    override fun calculateSalary() = 50000.0
}
