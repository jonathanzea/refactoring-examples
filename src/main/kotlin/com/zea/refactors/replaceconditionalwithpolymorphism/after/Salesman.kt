package com.zea.refactors.replaceconditionalwithpolymorphism.after

class Salesman : EmployeeType {
    override fun calculateSalary() = 20000.0
}
