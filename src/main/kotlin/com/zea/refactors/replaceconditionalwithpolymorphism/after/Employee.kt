package com.zea.refactors.replaceconditionalwithpolymorphism.after

class Employee(private val type: EmployeeType) {
    fun getSalary(): Double = type.calculateSalary()
}
