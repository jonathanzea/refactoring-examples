package com.zea.refactors.replaceconditionalwithpolymorphism.after

class Manager : EmployeeType {
    override fun calculateSalary() = 80000.0
}
