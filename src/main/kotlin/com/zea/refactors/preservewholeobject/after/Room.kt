package com.zea.refactors.preservewholeobject.after

class Room(val minimumTemperature: Int, val maximumTemperature: Int, private val heatingPlan: HeatingPlan) {

    fun isWithinPlanRange(): Boolean {
        return heatingPlan.isWithinTemperatureRange(this)
    }
}
