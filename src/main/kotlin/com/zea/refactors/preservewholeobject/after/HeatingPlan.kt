package com.zea.refactors.preservewholeobject.after

class HeatingPlan(private val temperatureRange: TemperatureRange) {

    fun isWithinTemperatureRange(room: Room): Boolean {
        return temperatureRange.isWithin(room.minimumTemperature, room.maximumTemperature)
    }
}
