package com.zea.refactors.preservewholeobject.after

class TemperatureRange(private val minimumTemperature: Int, private val maximumTemperature: Int) {
    fun isWithin(minimum: Int, maximum: Int) = minimum >= minimumTemperature && maximum <= maximumTemperature
}
