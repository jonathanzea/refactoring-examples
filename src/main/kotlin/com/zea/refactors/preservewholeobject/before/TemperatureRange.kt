package com.zea.refactors.preservewholeobject.before

class TemperatureRange(val minimumTemperature: Int, val maximumTemperature: Int)
