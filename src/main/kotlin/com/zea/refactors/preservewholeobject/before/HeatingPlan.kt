package com.zea.refactors.preservewholeobject.before

class HeatingPlan(private val temperatureRange: TemperatureRange) {

    fun isWithinTemperatureRange(low: Int, high: Int): Boolean {
        return low >= temperatureRange.minimumTemperature && high <= temperatureRange.maximumTemperature
    }
}
