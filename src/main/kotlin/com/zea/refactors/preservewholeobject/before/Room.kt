package com.zea.refactors.preservewholeobject.before

class Room(private val minimumTemperature: Int, private val maximumTemperature: Int, private val heatingPlan: HeatingPlan) {

    fun isWithinPlanRange(): Boolean {
        val low = minimumTemperature
        val high = maximumTemperature
        return heatingPlan.isWithinTemperatureRange(low, high)
    }
}
