package com.zea.refactors.extractsuperclass.before

class Employee(val name: String, val id: String, val annualCost: Double) {
    fun getSummary(): String {
        return "Employee: $name($id) - Annual Cost: $annualCost"
    }
}
