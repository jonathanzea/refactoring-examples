package com.zea.refactors.extractsuperclass.before

class Department(val name: String, val staff: List<Employee>) {
    fun printSummary(): String {
        val total: Double = staff.map { it.annualCost }.sum()
        return "Department: $name - Staff: ${staff.size} people - Annual Cost: $total"
    }
}
