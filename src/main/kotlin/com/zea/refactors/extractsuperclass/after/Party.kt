package com.zea.refactors.extractsuperclass.after

abstract class Party(val name: String) {
    abstract fun summary(): String
}
