package com.zea.refactors.extractsuperclass.after

class Department(name: String, private val staff: List<Employee>) : Party(name) {
    override fun summary(): String {
        val total: Double = staff.map { it.annualCost }.sum()
        return "Department: $name - Staff: ${staff.size} people - Annual Cost: $total"
    }
}
