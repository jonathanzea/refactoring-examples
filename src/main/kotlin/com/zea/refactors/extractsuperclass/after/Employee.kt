package com.zea.refactors.extractsuperclass.after

class Employee(name: String, private val id: String, val annualCost: Double) : Party(name) {
    override fun summary(): String {
        return "Employee: $name($id) - Annual Cost: $annualCost"
    }
}
