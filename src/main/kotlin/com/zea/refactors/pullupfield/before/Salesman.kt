package com.zea.refactors.pullupfield.before

class Salesman(private val name: String) : Employee() {
    fun printSummary() = "Salesman ($name)"
}
