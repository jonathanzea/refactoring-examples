package com.zea.refactors.pullupfield.before

class Engineer(private val name: String) : Employee() {
    fun printSummary() = "Engineer ($name) - Area: Computing"
}
