package com.zea.refactors.pullupfield.after

class Engineer(name: String) : Employee(name) {
    fun printSummary() = "Engineer ($name) - Area: Computing"
}
