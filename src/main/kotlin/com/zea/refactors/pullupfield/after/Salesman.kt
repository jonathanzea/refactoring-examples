package com.zea.refactors.pullupfield.after

class Salesman(name: String) : Employee(name) {
    fun printSummary() = "Salesman ($name)"
}
