package com.zea.refactors.replacerecordwithdataclass.before

class Vehicle(name: String, price: Double, version: String) {
    private val record = Array<Any>(3) {}

    init {
        this.record[0] = name
        this.record[1] = price
        this.record[2] = version
    }

    fun getName() = record[0] as String
    fun getPrice() = record[1] as Double
    fun getVersion() = record[2] as String
}
