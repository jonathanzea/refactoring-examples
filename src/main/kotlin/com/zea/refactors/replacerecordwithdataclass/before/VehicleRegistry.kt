package com.zea.refactors.replacerecordwithdataclass.before

class VehicleRegistry(private val vehicle: Vehicle) {
    fun printVehicleSummary() = "Summary = ${vehicle.getName()} - ${vehicle.getPrice()} - ${vehicle.getVersion()}"
}
