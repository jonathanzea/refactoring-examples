package com.zea.refactors.replacerecordwithdataclass.after

class VehicleRegistry(private val record: Array<Any>) {
    fun printVehicleSummary() = "Summary = ${record[0] as String} - ${record[1] as Double} - ${record[2] as String}"
}
