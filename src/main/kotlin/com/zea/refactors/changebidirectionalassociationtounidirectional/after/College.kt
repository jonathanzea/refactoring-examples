package com.zea.refactors.changebidirectionalassociationtounidirectional.after

data class College(val name: String) {
    fun getStudentNationality(student: Student): String = student.nationality
}
