package com.zea.refactors.changebidirectionalassociationtounidirectional.after

data class Student(val name: String, var college: College, val nationality: String = "American") {

    fun switchCollege(college: College) {
        this.college = college
    }
}
