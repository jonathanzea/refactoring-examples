package com.zea.refactors.changebidirectionalassociationtounidirectional.before

data class Student(val name: String, var college: College, val nationality: String = "American") {

    init {
        college.addStudent(this)
    }

    fun switchCollege(college: College) {
        this.college.students.remove(this)
        this.college = college
    }
}
