package com.zea.refactors.changebidirectionalassociationtounidirectional.before

data class College(val name: String) {

    var students: MutableSet<Student> = mutableSetOf()

    fun addStudent(student: Student) {
        this.students.add(student)
    }

    fun getStudentNationality(student: Student): String = students.first { it == student }.nationality
}
