package com.zea.refactors.replaceerrorcodewithexception.before

class Account(val id: Int, private val balance: Double) {

    fun withdraw(amount: Double): Int {
        if (amount > balance) {
            return -1
        } else {
            balance - amount
            return 0
        }
    }
}
