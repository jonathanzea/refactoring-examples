package com.zea.refactors.replaceerrorcodewithexception.before

class TransactionSimulator(private val account: Account) {

    fun simulateTransactionOf(amount: Double): String {
        if (account.withdraw(amount) == -1) {
            return handleOverdrawn()
        } else {
            return printCalculation()
        }
    }

    private fun handleOverdrawn(): String {
        return "Account (id: ${account.id}) has not sufficient funds"
    }

    private fun printCalculation(): String {
        return "SUCCESS - Transaction possible to do - Account (id: ${account.id})"
    }
}
