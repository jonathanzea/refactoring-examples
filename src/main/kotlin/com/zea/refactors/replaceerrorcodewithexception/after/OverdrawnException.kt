package com.zea.refactors.replaceerrorcodewithexception.after

import java.lang.RuntimeException

class OverdrawnException(message: String) : RuntimeException(message)
