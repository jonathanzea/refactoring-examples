package com.zea.refactors.replaceerrorcodewithexception.after

class Assert {
    companion object {
        fun that(condition: Boolean, message: String) {
            if (!condition)
                throw OverdrawnException(message)
        }
    }
}
