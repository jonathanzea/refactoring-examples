package com.zea.refactors.replaceerrorcodewithexception.after

class Account(val id: Int, val balance: Double) {

    fun withdraw(amount: Double): Double {
        Assert.that(
            amount < balance,
            "Amount too large"
        )
        return balance - amount
    }

    fun cannotWithdraw(amount: Double) = balance < amount
}
