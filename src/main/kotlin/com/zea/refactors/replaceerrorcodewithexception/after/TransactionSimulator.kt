package com.zea.refactors.replaceerrorcodewithexception.after

class TransactionSimulator(private var account: Account) {

    fun simulateTransactionOf(amount: Double): String {
        return if (account.cannotWithdraw(amount)) {
            handleOverdrawn()
        } else {
            printCalculation(account.withdraw(amount))
        }
    }

    private fun handleOverdrawn(): String {
        return "Account (id: ${account.id}) has not sufficient funds"
    }

    private fun printCalculation(newBalance: Double): String {
        return "SUCCESS - Transaction possible to do - Account (id: ${account.id}) new balance: €$newBalance"
    }
}
