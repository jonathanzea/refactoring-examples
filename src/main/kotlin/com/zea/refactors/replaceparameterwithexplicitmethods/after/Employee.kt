package com.zea.refactors.replaceparameterwithexplicitmethods.after

interface Employee {
    fun getType(): String
}
