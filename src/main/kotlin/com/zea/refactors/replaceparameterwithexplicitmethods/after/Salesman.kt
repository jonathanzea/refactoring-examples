package com.zea.refactors.replaceparameterwithexplicitmethods.after

class Salesman : Employee {
    override fun getType(): String {
        return EmployeeType.SALESMAN.key
    }
}
