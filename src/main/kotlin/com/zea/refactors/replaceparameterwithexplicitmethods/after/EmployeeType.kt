package com.zea.refactors.replaceparameterwithexplicitmethods.after

enum class EmployeeType(val key: String) {
    ENGINEER("engineer"),
    SALESMAN("salesman"),
    MANAGER("manager")
}
