package com.zea.refactors.replaceparameterwithexplicitmethods.after

class Manager : Employee {
    override fun getType(): String {
        return EmployeeType.MANAGER.key
    }
}
