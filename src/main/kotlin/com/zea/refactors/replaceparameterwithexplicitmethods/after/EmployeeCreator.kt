package com.zea.refactors.replaceparameterwithexplicitmethods.after

class EmployeeCreator {

    fun createEngineer(): Employee {
        return Engineer()
    }

    fun createSalesman(): Employee {
        return Salesman()
    }

    fun createManager(): Employee {
        return Manager()
    }
}
