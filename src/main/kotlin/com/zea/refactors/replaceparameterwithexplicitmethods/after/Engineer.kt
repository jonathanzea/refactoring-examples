package com.zea.refactors.replaceparameterwithexplicitmethods.after

class Engineer : Employee {
    override fun getType(): String {
        return EmployeeType.ENGINEER.key
    }
}
