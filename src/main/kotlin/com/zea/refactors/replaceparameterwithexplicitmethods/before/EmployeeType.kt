package com.zea.refactors.replaceparameterwithexplicitmethods.before

enum class EmployeeType(val key: String) {
    ENGINEER("engineer"),
    SALESMAN("salesman"),
    MANAGER("manager")
}
