package com.zea.refactors.replaceparameterwithexplicitmethods.before

import com.zea.refactors.replaceparameterwithexplicitmethods.after.Employee
import com.zea.refactors.replaceparameterwithexplicitmethods.after.EmployeeType
import com.zea.refactors.replaceparameterwithexplicitmethods.after.Engineer
import com.zea.refactors.replaceparameterwithexplicitmethods.after.Manager
import com.zea.refactors.replaceparameterwithexplicitmethods.after.Salesman

class EmployeeCreator(val type: EmployeeType) {

    fun create(): Employee {
        return when (type) {
            EmployeeType.ENGINEER -> Engineer()
            EmployeeType.SALESMAN -> Salesman()
            EmployeeType.MANAGER -> Manager()
        }
    }
}
