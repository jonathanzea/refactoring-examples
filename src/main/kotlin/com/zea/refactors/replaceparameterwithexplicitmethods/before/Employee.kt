package com.zea.refactors.replaceparameterwithexplicitmethods.before

interface Employee {
    fun getType(): String
}
