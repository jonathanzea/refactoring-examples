package com.zea.refactors.replaceparameterwithexplicitmethods.before

import com.zea.refactors.replaceparameterwithexplicitmethods.after.Employee
import com.zea.refactors.replaceparameterwithexplicitmethods.after.EmployeeType

class Salesman : Employee {
    override fun getType(): String {
        return EmployeeType.SALESMAN.key
    }
}
