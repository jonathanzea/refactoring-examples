package com.zea.refactors.replaceparameterwithmethod.after

class Order(private val productName: String, private val quantity: Int, private val price: Double) {

    fun calculate() =
        "$productName ($quantity) - €${discountedPrice(quantity * price)}"

    private fun discountedPrice(basePrice: Double) =
        if (getDiscountLevel(quantity) == 2) basePrice * .01
        else basePrice * .05

    private fun getDiscountLevel(quantity: Int) = if (quantity > 100) 2 else 1
}
