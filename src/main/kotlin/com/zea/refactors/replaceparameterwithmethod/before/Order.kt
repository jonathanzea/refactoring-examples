package com.zea.refactors.replaceparameterwithmethod.before

class Order(private val productName: String, private val quantity: Int, private val price: Double) {

    fun calculate(): String {
        val basePrice = quantity * price
        var discountLevel = 1
        if (quantity > 100) discountLevel = 2
        val finalPrice = discountedPrice(basePrice, discountLevel)
        return "$productName ($quantity) - €$finalPrice"
    }

    private fun discountedPrice(basePrice: Double, discountLevel: Int): Double {
        return if (discountLevel == 2) basePrice * .01
        else basePrice * .05
    }
}
