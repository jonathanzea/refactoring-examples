package com.zea.refactors.introducelocalextension.before

import java.util.Date

class DateHelper(private val date: Date) {
    fun tomorrowsDayCode() = date.day + 1
    fun yesterdaysDayCode() = date.day - 1
}
