package com.zea.refactors.introducelocalextension.after

import java.util.Date

class SpecializedDate : Date() {
    fun tomorrowsDayCode() = Date().day + 1
    fun yesterdaysDayCode() = Date().day - 1
}
