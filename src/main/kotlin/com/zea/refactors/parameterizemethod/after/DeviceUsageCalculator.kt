package com.zea.refactors.parameterizemethod.after

class DeviceUsageCalculator(private val usagePoints: Int) {

    fun calculate(): Double {
        var result = usagePoints.coerceAtMost(100) * .03
        result += useInRange(100, 200, .05)
        result += useInRange(200, 200, .07)
        return result
    }

    private fun useInRange(start: Int, end: Int, factor: Double): Double {
        return if (usagePoints > start)
            usagePoints.coerceAtMost(end) - 100 * factor
        else
            0.0
    }
}
