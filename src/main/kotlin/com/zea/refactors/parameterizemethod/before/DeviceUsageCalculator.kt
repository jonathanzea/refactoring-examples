package com.zea.refactors.parameterizemethod.before

class DeviceUsageCalculator(private val usagePoints: Int) {

    fun calculate(): Double {
        var result = usagePoints.coerceAtMost(100) * .03
        if (usagePoints > 100) {
            result += usagePoints.coerceAtMost(200) - 100 * .05
        }
        if (usagePoints > 200) {
            result += usagePoints.coerceAtMost(200) - 100 * .07
        }
        return result
    }
}
