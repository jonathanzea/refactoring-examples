package com.zea.refactors.introduceassertion.after

class Customer(age: Int, val name: String) {
    init {
        require(age > 18) { "Customers cannot be underage" }
    }
}
