package com.zea.refactors.introduceassertion.before

import java.lang.IllegalArgumentException

class Customer(val age: Int, val name: String) {
    init {
        if (this.age < 18)
            throw IllegalArgumentException("Customers cannot be underage")
    }
}
