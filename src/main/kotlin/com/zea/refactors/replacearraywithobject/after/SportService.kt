package com.zea.refactors.replacearraywithobject.after

class SportService(private val team: SoccerTeam) {

    fun teamName() = team.name
    fun teamWins() = team.wins
    fun teamLoses() = team.loses
}
