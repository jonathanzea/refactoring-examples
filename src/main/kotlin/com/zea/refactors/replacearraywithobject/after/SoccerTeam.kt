package com.zea.refactors.replacearraywithobject.after

class SoccerTeam(val name: String, val wins: Int, val loses: Int)
