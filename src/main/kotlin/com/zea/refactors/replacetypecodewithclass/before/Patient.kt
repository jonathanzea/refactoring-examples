package com.zea.refactors.replacetypecodewithclass.before

class Patient(val name: String, val bloodGroup: Int) {

    companion object {
        const val O = 0
        const val A = 1
        const val B = 2
        const val AB = 3
    }

    fun printPatientInfo() = "$name - Blood Type: $bloodGroup"
}
