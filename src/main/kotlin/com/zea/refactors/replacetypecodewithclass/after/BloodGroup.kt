package com.zea.refactors.replacetypecodewithclass.after

enum class BloodGroup(val code: Int) {
    O(0),
    A(1),
    B(2),
    AB(3)
}
