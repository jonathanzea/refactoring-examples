package com.zea.refactors.replacetypecodewithclass.after

class Patient(private val name: String, private val bloodGroup: Int) {
    fun printPatientInfo() = "$name - Blood Type: $bloodGroup"
}
