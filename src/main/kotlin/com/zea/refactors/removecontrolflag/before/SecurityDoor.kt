package com.zea.refactors.removecontrolflag.before

class SecurityDoor {
    private var alerts = 0
    private var alarm = ""

    fun passing(people: List<String>) {
        var found = ""
        people.forEach {
            if (it == "John") {
                found = "John"
                sendAlert()
            }
            if (it == "Don") {
                found = "Don"
                sendAlert()
            }
        }
        alarm(found)
    }

    private fun alarm(found: String) {
        alarm = "ALARM! Blacklisted detected: $found"
    }

    private fun sendAlert() {
        alerts++
    }

    fun lastAlarm() = alarm
    fun countAlerts() = alerts
}
