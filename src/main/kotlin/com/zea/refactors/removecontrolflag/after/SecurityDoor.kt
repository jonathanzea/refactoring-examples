package com.zea.refactors.removecontrolflag.after

class SecurityDoor {
    private var alerts = 0
    private var alarm = ""

    fun checkSecurity(people: List<String>) {
        val found = foundBlacklisted(people)
        alarm(found)
    }

    private fun foundBlacklisted(people: List<String>): String {
        people.forEach {
            if (it == "John") {
                sendAlert()
                return "John"
            }
            if (it == "Don") {
                sendAlert()
                return "Don"
            }
        }
        return ""
    }

    private fun alarm(found: String) {
        alarm = "ALARM! Blacklisted detected: $found"
    }

    private fun sendAlert() {
        alerts++
    }

    fun lastAlarm() = alarm
    fun countAlerts() = alerts
}
