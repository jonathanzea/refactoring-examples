package com.zea.refactors.encapsulatedowncast.before

class TreeFamily(val trees: List<BluePine>) {
    fun getLastPineParentSpecie(): Any {
        return trees.last()
    }
}
