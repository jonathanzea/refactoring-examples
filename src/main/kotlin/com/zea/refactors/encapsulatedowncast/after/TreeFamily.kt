package com.zea.refactors.encapsulatedowncast.after

class TreeFamily(private val trees: List<BluePine>) {
    fun getLastPineParentSpecie() = trees.last() as Pine
}
