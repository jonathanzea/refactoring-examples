package com.zea.refactors.replacedelegationwithinheritance.before

class Person(val name: String, val position: String, val country: String)
