package com.zea.refactors.replacedelegationwithinheritance.before

class Employee(private val person: Person) {

    fun getSummary(): String {
        return "Employee: ${person.name} (${person.position}) - ${person.country.toUpperCase()}"
    }
}
