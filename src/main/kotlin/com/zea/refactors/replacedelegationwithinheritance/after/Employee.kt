package com.zea.refactors.replacedelegationwithinheritance.after

class Employee(name: String, position: String, country: String) :
    Person(name, position, country) {

    fun getSummary() = "Employee: $name ($position) - ${country.toUpperCase()}"
}
