package com.zea.refactors.replacedelegationwithinheritance.after

open class Person(val name: String, val position: String, val country: String)
