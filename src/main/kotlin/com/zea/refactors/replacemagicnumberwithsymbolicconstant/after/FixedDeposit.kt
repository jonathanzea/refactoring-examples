package com.zea.refactors.replacemagicnumberwithsymbolicconstant.after

import com.zea.refactors.changeunidirectionalassociationtobidirectional.after.Customer
import java.math.RoundingMode

class FixedDeposit(val customer: Customer, val amount: Double, val days: Int) {

    companion object {
        const val FIXED_DEPOSIT_YEARLY_RATE = 0.01
    }

    fun calculate(): String {
        return "Interests: €${((FIXED_DEPOSIT_YEARLY_RATE / 365 * days) * amount).toBigDecimal().setScale(
            2,
            RoundingMode.HALF_EVEN
        )}"
    }
}
