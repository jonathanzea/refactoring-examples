package com.zea.refactors.replacemagicnumberwithsymbolicconstant.before

import com.zea.refactors.changeunidirectionalassociationtobidirectional.after.Customer
import java.math.RoundingMode

class FixedDeposit(val customer: Customer, val amount: Double, val days: Int) {

    fun calculate(): String {
        return "Interests: €${((0.01 / 365 * days) * amount).toBigDecimal().setScale(2, RoundingMode.HALF_EVEN)}"
    }
}
