package com.zea.refactors.separatequeryfrommodifier.after

class SecurityService(private vararg val persons: String) {

    var alertReport = ""
    var alarmTriggered = false

    fun checkSecurity() {
        val person = findPerson(listOf(*persons))
        scanSystem(person)
        printAlertReport(person)
    }

    private fun findPerson(persons: List<String>): String {
        try {
            return persons.first { it == "Marivi" || it == "Edda" }
        } catch (e: NoSuchElementException) {
        }
        return ""
    }

    private fun scanSystem(person: String) {
        if (person != "") {
            triggerAlarm()
        }
    }

    private fun triggerAlarm() {
        alarmTriggered = true
    }

    private fun printAlertReport(miscreant: String) {
        alertReport = if (miscreant != "")
            "Miscreant Found: $miscreant"
        else
            "No Miscreant Found"
    }
}
