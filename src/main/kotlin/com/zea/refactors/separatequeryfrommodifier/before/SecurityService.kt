package com.zea.refactors.separatequeryfrommodifier.before

class SecurityService(vararg val persons: String) {

    var alertReport = ""
    var alarmTriggered = false

    fun checkSecurity() {
        val found = findMiscreant(listOf(*persons))
        printAlertReport(found)
    }

    private fun findMiscreant(persons: List<String>): String {
        for (person in persons) {
            if (person == "Marivi") {
                triggerAlarm()
                return "Marivi"
            }
            if (person == "Edda") {
                triggerAlarm()
                return "Edda"
            }
        }
        return ""
    }

    private fun triggerAlarm() {
        alarmTriggered = true
    }

    private fun printAlertReport(miscreant: String) {
        if (miscreant != "")
            alertReport = "Miscreant Found: $miscreant"
        else
            alertReport = "No Miscreant Found"
    }
}
