package com.zea.refactors.consolidateconditionalexpression.after

class Customer(private val hasADisability: Boolean, private val age: Int, private val status: String) {

    fun getTicketPrice() = if (isSelectableForLowPrice()) 10.00 else 25.00

    private fun isSelectableForLowPrice(): Boolean = hasADisability || status == "student" || age < 12
}
