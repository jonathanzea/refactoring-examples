package com.zea.refactors.consolidateconditionalexpression.before

class Customer(val hasADisability: Boolean, val age: Int, val status: String) {

    fun getTicketPrice(): Double {
        if (hasADisability) {
            return 10.0
        }
        if (status == "student") {
            return 10.00
        }

        if (age < 12) {
            return 10.00
        } else
            return 25.00
    }
}
