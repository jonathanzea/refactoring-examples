package com.zea.refactors.movefield.after

class Account(private val accountType: AccountType) {

    fun interestsFor(amount: Double, days: Int): String {
        return "${accountType.identifier} - interests: €${(amount * ((accountType.yearlyInterestRate / 100) / 365) * days)}"
    }
}
