package com.zea.refactors.movefield.after

class AccountType(val identifier: String, val yearlyInterestRate: Double)
