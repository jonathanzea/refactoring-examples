package com.zea.refactors.movefield.before

class Account(val accountType: AccountType, val yearlyInterestRate: Double) {

    fun interestsFor(amount: Double, days: Int): String {
        return "${accountType.identifier} - interests: €${(amount * (((yearlyInterestRate / 100) / 365) * days))}"
    }
}
