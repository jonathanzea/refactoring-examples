package com.zea.refactors.replacesubclasswithfields.after

class Person(val isMale: Boolean, val code: Char) {

    companion object {
        fun createMale(): Person {
            return Person(true, 'M')
        }

        fun createFemale(): Person {
            return Person(false, 'F')
        }
    }
}
