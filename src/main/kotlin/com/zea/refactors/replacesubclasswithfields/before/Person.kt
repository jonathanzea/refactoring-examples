package com.zea.refactors.replacesubclasswithfields.before

abstract class Person {
    abstract fun isMale(): Boolean
    abstract fun getCode(): Char
}
