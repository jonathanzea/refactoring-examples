package com.zea.refactors.replacesubclasswithfields.before

class Male : Person() {

    override fun getCode(): Char {
        return 'M'
    }

    override fun isMale(): Boolean {
        return true
    }
}
