package com.zea.refactors.replacesubclasswithfields.before

class Female : Person() {

    override fun getCode(): Char {
        return 'F'
    }

    override fun isMale(): Boolean {
        return false
    }
}
