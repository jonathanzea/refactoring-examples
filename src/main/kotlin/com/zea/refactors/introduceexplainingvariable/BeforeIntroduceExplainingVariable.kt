package com.zea.refactors.introduceexplainingvariable

import kotlin.math.max
import kotlin.math.min

class BeforeIntroduceExplainingVariable(var quantity: Int, var price: Double) {

    fun calculatePrice(): Double {
        // price is: base price + quantity discount + shipping
        return quantity * price - max(0, quantity - 500) * price * .005 + min(quantity * price * .1, 100.0)
    }
}
