package com.zea.refactors.introduceexplainingvariable

import kotlin.math.max
import kotlin.math.min

class AfterIntroduceExplainingVariable(private val quantity: Int, private val price: Double) {

    fun calculatePrice(): Double {
        val basePrice = quantity * price
        val quantityDiscount = max(0, quantity - 500) * price * .005
        val shipping = min(basePrice * .1, 100.0)
        return basePrice - quantityDiscount + shipping
    }
}
