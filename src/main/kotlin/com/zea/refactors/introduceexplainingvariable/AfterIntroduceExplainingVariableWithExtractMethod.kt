package com.zea.refactors.introduceexplainingvariable

import kotlin.math.max
import kotlin.math.min

class AfterIntroduceExplainingVariableWithExtractMethod(private val quantity: Int, private val price: Double) {

    fun calculatePrice(): Double {
        return calculateBasePrice() - calculateQuantityDiscount() + calculateShipping(calculateBasePrice())
    }

    private fun calculateShipping(basePrice: Double): Double {
        return min(basePrice * .1, 100.0)
    }

    private fun calculateQuantityDiscount(): Double {
        return max(0, quantity - 500) * price * .005
    }

    private fun calculateBasePrice(): Double {
        return quantity * price
    }
}
