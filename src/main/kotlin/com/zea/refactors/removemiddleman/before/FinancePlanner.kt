package com.zea.refactors.removemiddleman.before

class FinancePlanner(private val name: String, private val company: ITCompany) {

    fun calculateYearlySalary() = "$name -> €${company.getYearlySalary()}"
}
