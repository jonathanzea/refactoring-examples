package com.zea.refactors.substitutealgorithm

class AfterSubstituteAlgorithm(vararg val persons: String) {

    fun find(name: String): String = if (name in persons) {
        "FOUND: Persons[${persons.indexOf(name)}]=$name"
    } else {
        "NOT FOUND"
    }
}
