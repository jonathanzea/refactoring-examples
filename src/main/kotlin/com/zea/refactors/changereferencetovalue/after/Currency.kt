package com.zea.refactors.changereferencetovalue.after

data class Currency(val code: String, val exchangeRate: Double)
