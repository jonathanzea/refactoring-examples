package com.zea.refactors.changereferencetovalue.before

class Currency private constructor(val exchangeRate: Double) {

    companion object {
        private val codes: Map<String, Double> = mapOf("USD" to 1.1,
            "ARS" to 160.0,
            "EUR" to 1.1,
            "BSF" to 360000.5,
            "CZK" to 22.2)

        fun getExchangeRate(code: String): Currency {
            return Currency(codes[code]!!)
        }
    }
}
