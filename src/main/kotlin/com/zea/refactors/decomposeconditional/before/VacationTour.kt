package com.zea.refactors.decomposeconditional.before

import java.time.LocalDate
import java.time.Month

class VacationTour(val date: LocalDate, val nights: Int, val destination: String) {

    companion object {
        val SUMMER_START = Month.JULY
        val SUMMER_ENDS = Month.OCTOBER

        const val WINTER_RATE = 10.0
        const val SUMMER_RATE = 20.0
    }

    private fun calculateCharge(): Double {
        if (date.month <= SUMMER_START || date.month >= SUMMER_ENDS)
            return WINTER_RATE * nights
        else
            return SUMMER_RATE * nights
    }

    fun printItinerary(): String {
        return "Trip to: $destination - Price: €${calculateCharge()}"
    }
}
