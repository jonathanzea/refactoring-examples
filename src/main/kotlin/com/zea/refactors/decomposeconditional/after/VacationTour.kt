package com.zea.refactors.decomposeconditional.after

import java.time.LocalDate
import java.time.Month

class VacationTour(val date: LocalDate, val nights: Int, val destination: String) {

    companion object {
        val SUMMER_START = Month.JULY
        val SUMMER_ENDS = Month.OCTOBER
        const val WINTER_RATE = 10.0
        const val SUMMER_RATE = 20.0
    }

    fun printItinerary(): String {
        return "Trip to: $destination - Price: €${calculateCharge()}"
    }

    private fun calculateCharge() =
        if (notSummer(date))
            winterCharge(nights)
        else
            summerCharge(nights)

    private fun notSummer(date: LocalDate) = date.month <= SUMMER_START || date.month >= SUMMER_ENDS
    private fun winterCharge(nights: Int) = nights * WINTER_RATE
    private fun summerCharge(nights: Int) = nights * SUMMER_RATE
}
