package com.zea.refactors.extractsubclass.before

class JobItem(
    private val unitPrice: Int,
    private val quantity: Int,
    private val hasDiscount: Boolean,
    val employee: Employee
) {
    fun getTotalPrice() = unitPrice * quantity

    fun getUnitPrice() = if (hasDiscount) {
        employee.discountPrice
    } else {
        unitPrice
    }
}
