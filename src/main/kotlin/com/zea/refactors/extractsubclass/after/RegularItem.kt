package com.zea.refactors.extractsubclass.after

open class RegularItem(private val unitPrice: Int, private val quantity: Int) {

    fun getTotalPrice() = unitPrice * quantity

    open fun getUnitPrice() = unitPrice
}
