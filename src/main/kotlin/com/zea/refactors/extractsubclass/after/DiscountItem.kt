package com.zea.refactors.extractsubclass.after

class DiscountItem(unitPrice: Int, quantity: Int, val employee: Employee) : RegularItem(unitPrice, quantity) {
    override fun getUnitPrice() = employee.discountPrice
}
