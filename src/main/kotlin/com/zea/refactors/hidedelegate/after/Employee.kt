package com.zea.refactors.hidedelegate.after

class Employee(val name: String, private val department: Department) {

    fun getManager() = department.getManager()
}
