package com.zea.refactors.hidedelegate.after

class Department(val name: String) {

    fun getManager() = "Iris"
}
