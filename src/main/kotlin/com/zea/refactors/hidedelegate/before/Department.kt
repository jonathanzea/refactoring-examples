package com.zea.refactors.hidedelegate.before

class Department(val name: String) {

    fun getManager() = "Iris"
}
