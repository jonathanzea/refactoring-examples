package com.zea.refactors.extracthierarchy.before

open class BillingPlan {
    open fun calculate(customer: Customer): Double {
        return if (customer.isDisabled) {
            50.0
        } else
            100.0
    }
}
