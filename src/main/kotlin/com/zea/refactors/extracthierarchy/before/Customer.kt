package com.zea.refactors.extracthierarchy.before

class Customer(val name: String, val isDisabled: Boolean) {

    private val billingPlan = BillingPlan()

    fun printBillingPlan(): String {
        val billingAmount = billingPlan.calculate(this)
        return "Customer: $name - Billing Plan: $$billingAmount/month"
    }
}
