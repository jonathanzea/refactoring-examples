package com.zea.refactors.extracthierarchy.after

open class BillingPlan {

    companion object {
        fun createFor(customer: Customer) =
            if (customer.isDisabled)
                DisabledCustomerBillingPlan()
            else
                BillingPlan()
    }

    open fun calculate(): Double {
        return 100.0
    }
}
