package com.zea.refactors.extracthierarchy.after

class Customer(val name: String, val isDisabled: Boolean) {

    private val billingPlan = BillingPlan.createFor(this)

    fun printBillingPlan(): String {
        return "Customer: $name - Billing Plan: $${billingPlan.calculate()}/month"
    }
}
