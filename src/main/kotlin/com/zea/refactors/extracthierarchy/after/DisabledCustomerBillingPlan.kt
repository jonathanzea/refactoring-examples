package com.zea.refactors.extracthierarchy.after

class DisabledCustomerBillingPlan : BillingPlan() {
    override fun calculate(): Double {
        return 50.0
    }
}
