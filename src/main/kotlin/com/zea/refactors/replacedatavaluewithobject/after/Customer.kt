package com.zea.refactors.replacedatavaluewithobject.after

class Customer(val name: String) {
    fun getFirstNameUppercased() = name.split(' ')[0].toUpperCase()
}
