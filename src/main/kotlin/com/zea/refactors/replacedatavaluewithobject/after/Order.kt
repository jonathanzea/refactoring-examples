package com.zea.refactors.replacedatavaluewithobject.after

class Order(private val price: Double, private val amount: Int, private val customer: Customer) {

    fun print() = "${customer.getFirstNameUppercased()} - ($amount) items - Total: €$price"
}
