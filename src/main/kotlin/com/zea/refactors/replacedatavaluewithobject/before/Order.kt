package com.zea.refactors.replacedatavaluewithobject.before

class Order(private val price: Double, private val amount: Int, private val customer: String) {

    fun print(): String {
        val customerUppercaseName = uppercaseFirstName()
        return "$customerUppercaseName - ($amount) items - Total: €$price"
    }

    private fun uppercaseFirstName() = customer.split(' ')[0].toUpperCase()
}
