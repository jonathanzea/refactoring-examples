package com.zea.refactors.extractclass.before

class Person(val name: String, val countryCode: String, val areaCode: String, val phoneNumber: String) {

    fun printContact(): String = "$name \nTelefonnummer: ${phoneNumber()}"

    private fun phoneNumber(): String = "$countryCode - $areaCode - $phoneNumber"
}
