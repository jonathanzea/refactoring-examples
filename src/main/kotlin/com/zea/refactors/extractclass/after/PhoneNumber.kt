package com.zea.refactors.extractclass.after

class PhoneNumber(private val countryCode: String, private val areaCode: String, private val number: String) {
    fun get() = "$countryCode - $areaCode - $number"
}
