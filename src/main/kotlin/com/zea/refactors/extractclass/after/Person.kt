package com.zea.refactors.extractclass.after

class Person(private val name: String, private val phoneNumber: PhoneNumber) {

    fun printContact(): String = "$name \nTelefonnummer: ${phoneNumber.get()}"
}
