package com.zea.refactors.changevaluetoreference.before

class OrderService(private val customerCatalog: List<Customer>) {

    fun numberOfOrdersFor(name: String): Int {
        var orders = 0
        customerCatalog.forEach {
            if (it.name == name) {
                orders += it.getNumberOfOrders()
            }
        }
        return orders
    }
}
