package com.zea.refactors.changevaluetoreference.before

class Customer(val name: String) {

    private var orders: Int = 0
    fun addOrder() = orders++
    fun getNumberOfOrders() = orders
}
