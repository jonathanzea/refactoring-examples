package com.zea.refactors.changevaluetoreference.after

class OrderService(private val customerCatalog: Set<Customer>) {
    fun numberOfOrdersFor(name: String) = customerCatalog.first { it == Customer.create(name) }.getNumberOfOrders()
}
