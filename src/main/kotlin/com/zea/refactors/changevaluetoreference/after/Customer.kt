package com.zea.refactors.changevaluetoreference.after

data class Customer private constructor(val name: String) {

    private var orders: Int = 0

    companion object {
        fun create(name: String): Customer {
            return Customer(name)
        }
    }

    fun addOrder() = orders++
    fun getNumberOfOrders() = orders
}
