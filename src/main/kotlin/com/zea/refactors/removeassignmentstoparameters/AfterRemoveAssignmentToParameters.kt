package com.zea.refactors.removeassignmentstoparameters

class AfterRemoveAssignmentToParameters {

    fun calculateDiscountFor(initialDiscount: Int, quantity: Int, yearsOfCustomerContract: Int): String {
        var result = initialDiscount
        if (initialDiscount > 50) result += 10
        if (quantity > 100) result += 10
        if (yearsOfCustomerContract > 5) result += 25
        return "Customer discount € $result.00"
    }
}
