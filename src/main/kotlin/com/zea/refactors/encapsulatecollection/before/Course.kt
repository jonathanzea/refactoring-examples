package com.zea.refactors.encapsulatecollection.before

class Course(val name: String, val monthlyCost: Double)
