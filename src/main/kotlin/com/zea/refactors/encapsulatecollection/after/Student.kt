package com.zea.refactors.encapsulatecollection.after

class Student(name: String) {
    private val courses = mutableListOf<Course>()

    fun addAllCourses(courses: List<Course>) {
        this.courses.addAll(courses)
    }

    fun addCourse(course: Course) {
        this.courses.add(course)
    }

    fun getCourses(): List<Course> {
        return this.courses
    }

    fun removeCourse(courseName: String) {
        this.courses.removeIf { it.name == courseName }
    }
}
