package com.zea.refactors.encapsulatecollection.after

class Course(val name: String, val monthlyCost: Double)
