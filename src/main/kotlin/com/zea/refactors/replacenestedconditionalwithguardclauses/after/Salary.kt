package com.zea.refactors.replacenestedconditionalwithguardclauses.after

class Salary(private val yearlyAmount: Double) {

    fun calculateMonthlyTaxDeduction(): Double {
        if (yearlyAmount > 50000.0) return deductionFor(.4)
        if (yearlyAmount > 30000.0) return deductionFor(.3)
        return 0.0
    }

    private fun deductionFor(percentage: Double): Double = yearlyAmount * percentage
}
