package com.zea.refactors.replacenestedconditionalwithguardclauses.before

class Salary(private val yearlyAmount: Double) {

    fun calculateMonthlyTaxDeduction(): Double {
        var taxDeduction = 0.0

        if (yearlyAmount > 30000.0) {
            if (yearlyAmount > 50000.0) {
                taxDeduction = yearlyAmount * .4
            } else {
                taxDeduction = yearlyAmount * .3
            }
        }
        return taxDeduction
    }
}
