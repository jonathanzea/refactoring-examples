package com.zea.refactors.replacemethodwithmethodobject

class PolicySummary(
    private val policyCenter: AfterReplaceMethodWithMethodObject,
    private val insuredName: String,
    private val paymentMethod: String,
    private val instalmentQuantity: Int,
    private val instalmentAmount: Double
) {

    fun compute(): String {
        return "{\"insured_name\":\"$insuredName\"," +
                "\"payment_method\":\"$paymentMethod\"," +
                "\"instalments\":$instalmentQuantity," +
                "\"for\":\"${policyCenter.insuranceType}\"" +
                "\"instalment_amount\":$instalmentAmount" +
                "}"
    }
}
