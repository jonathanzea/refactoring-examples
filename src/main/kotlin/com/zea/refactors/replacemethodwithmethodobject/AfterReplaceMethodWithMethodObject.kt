package com.zea.refactors.replacemethodwithmethodobject

class AfterReplaceMethodWithMethodObject(val insuranceType: String) {

    fun printPolicySummary(
        insuredName: String,
        paymentMethod: String,
        instalmentQuantity: Int,
        instalmentAmount: Double
    ): String {
        return PolicySummary(
            this,
            insuredName,
            paymentMethod,
            instalmentQuantity,
            instalmentAmount
        ).compute()
    }
}
