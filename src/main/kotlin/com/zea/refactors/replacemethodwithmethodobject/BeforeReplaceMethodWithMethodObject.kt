package com.zea.refactors.replacemethodwithmethodobject

class BeforeReplaceMethodWithMethodObject(val insuranceType: String) {

    fun printPolicySummary(
        insuredName: String,
        paymentMethod: String,
        instalmentQuantity: Int,
        instalmentAmount: Double
    ): String {
        return "{\"insured_name\":\"$insuredName\"," +
                "\"payment_method\":\"$paymentMethod\"," +
                "\"instalments\":$instalmentQuantity," +
                "\"for\":\"$insuranceType\"" +
                "\"instalment_amount\":$instalmentAmount" +
                "}"
    }
}
