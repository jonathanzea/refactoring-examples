package com.zea.refactors.replacetypecodewithsubclasses.after

class Manager : Employee() {
    override fun getType(): Int {
        return MANAGER
    }
}
