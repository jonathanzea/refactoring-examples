package com.zea.refactors.replacetypecodewithsubclasses.after

class Engineer : Employee() {
    override fun getType(): Int {
        return ENGINEER
    }
}
