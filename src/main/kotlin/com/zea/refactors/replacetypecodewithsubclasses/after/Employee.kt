package com.zea.refactors.replacetypecodewithsubclasses.after

import java.lang.IllegalArgumentException

abstract class Employee {
    abstract fun getType(): Int

    companion object {
        const val ENGINEER = 1
        const val SALESMAN = 2
        const val MANAGER = 3

        fun create(career: Int): Employee {
            return when (career) {
                ENGINEER -> Engineer()
                SALESMAN -> Salesman()
                MANAGER -> Manager()
                else -> throw IllegalArgumentException("Unknown career code: $career")
            }
        }
    }
}
