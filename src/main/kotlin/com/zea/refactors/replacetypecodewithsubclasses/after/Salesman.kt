package com.zea.refactors.replacetypecodewithsubclasses.after

class Salesman : Employee() {
    override fun getType(): Int {
        return SALESMAN
    }
}
