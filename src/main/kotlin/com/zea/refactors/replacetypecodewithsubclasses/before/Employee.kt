package com.zea.refactors.replacetypecodewithsubclasses.before

class Employee(val profession: Int) {

    companion object {
        const val ENGINEER = 1
        const val SALESMAN = 2
        const val MANAGER = 3
    }
}
