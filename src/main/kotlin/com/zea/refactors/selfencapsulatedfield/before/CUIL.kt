package com.zea.refactors.selfencapsulatedfield.before

class CUIL(val dni: DNI) {

    fun getCuil() = "20-${dni.id}-2"
}
