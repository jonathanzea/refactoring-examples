package com.zea.refactors.selfencapsulatedfield.after

class CUIL(dni: String) : DNI(dni) {

    override var id: String = dni
        get() {
            return "20-${super.id}-2"
        }
}
