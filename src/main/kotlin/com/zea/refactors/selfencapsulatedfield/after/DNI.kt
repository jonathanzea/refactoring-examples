package com.zea.refactors.selfencapsulatedfield.after

/*
In this example the encapsulation of the field access in getters and setters
* is been shown with one of it's advantages which is the modification
* of the field accessing when subclassing the object and creating as a result
* a lazy initialization like behaviour
*/
open class DNI(id: String) {

    open var id: String = id
        get() {
            return field
        }
        set(value) {
            field = value
        }
}
