package com.zea.refactors.replacetypecodewithstrategy.before

import java.lang.IllegalArgumentException

class Employee(val position: Int, val monthlySalary: Double, val bonus: Double, val comission: Double) {

    companion object {
        const val ENGINEER = 1
        const val MANAGER = 2
        const val SALESMAN = 3
    }

    fun paySalary(): Double {
        return when (position) {
            ENGINEER -> monthlySalary
            MANAGER -> monthlySalary + bonus
            SALESMAN -> monthlySalary + comission
            else -> throw IllegalArgumentException("Unknown employee code: $position")
        }
    }
}
