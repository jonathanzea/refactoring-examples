package com.zea.refactors.replacetypecodewithstrategy.after

class Engineer : EmployeeType() {
    override fun getTypeCode(): Int {
        return Employee.ENGINEER
    }
}
