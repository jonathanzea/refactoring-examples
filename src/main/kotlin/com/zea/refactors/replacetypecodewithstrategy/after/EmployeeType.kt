package com.zea.refactors.replacetypecodewithstrategy.after

abstract class EmployeeType {
    abstract fun getTypeCode(): Int
}
