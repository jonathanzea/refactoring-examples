package com.zea.refactors.replacetypecodewithstrategy.after

class Manager : EmployeeType() {
    override fun getTypeCode(): Int {
        return Employee.MANAGER
    }
}
