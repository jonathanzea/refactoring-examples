package com.zea.refactors.replacetypecodewithstrategy.after

class Salesman : EmployeeType() {
    override fun getTypeCode(): Int {
        return Employee.SALESMAN
    }
}
