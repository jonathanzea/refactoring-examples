package com.zea.refactors.replacetypecodewithstrategy.after

import java.lang.IllegalArgumentException

class Employee(val monthlySalary: Double, val bonus: Double, val comission: Double, val employeeType: EmployeeType) {

    companion object {
        const val ENGINEER = 1
        const val MANAGER = 2
        const val SALESMAN = 3
    }

    fun paySalary(): Double {
        return when (employeeType.getTypeCode()) {
            ENGINEER -> monthlySalary
            MANAGER -> monthlySalary + bonus
            SALESMAN -> monthlySalary + comission
            else -> throw IllegalArgumentException("Unknown employee code: ${employeeType.getTypeCode()}")
        }
    }
}
