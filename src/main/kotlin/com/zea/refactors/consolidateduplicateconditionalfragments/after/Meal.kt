package com.zea.refactors.consolidateduplicateconditionalfragments.after

class Meal(private val dishName: String) {

    private var price: Double = 0.0
    private val dealMenu = listOf("Meat Balls")

    fun getBill(): String {
        if (dealMenu.contains(dishName))
            price = 7.0
        else if (!dealMenu.contains(dishName))
            price = 13.4
        return print()
    }

    private fun print() = "$dishName - x1 - €$price"
}
