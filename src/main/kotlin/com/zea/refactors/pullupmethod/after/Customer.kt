package com.zea.refactors.pullupmethod.after

import java.time.LocalDate

abstract class Customer(private val lastBillDate: LocalDate) {
    private val bills = mutableMapOf<LocalDate, Double>()

    protected abstract fun chargeFor(startDate: LocalDate, endDate: LocalDate): Double

    fun createBill(date: LocalDate) {
        val amount = chargeFor(lastBillDate, date)
        addBill(date, amount)
    }

    private fun addBill(date: LocalDate, amount: Double) {
        bills[date] = amount
    }

    fun bills() = bills
}
