package com.zea.refactors.pullupmethod.after

import java.time.LocalDate
import java.time.temporal.ChronoUnit

class RegularCustomer(lastBillDate: LocalDate) : Customer(lastBillDate) {
    override fun chargeFor(startDate: LocalDate, endDate: LocalDate): Double {
        return if (startDate.until(endDate, ChronoUnit.DAYS) >= 10) {
            200.0
        } else {
            100.0
        }
    }
}
