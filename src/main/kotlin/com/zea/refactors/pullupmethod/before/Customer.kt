package com.zea.refactors.pullupmethod.before

import java.time.LocalDate

open class Customer(val lastBillDate: LocalDate) {
    private val bills = mutableMapOf<LocalDate, Double>()

    fun addBill(date: LocalDate, amount: Double) {
        bills[date] = amount
    }

    fun bills() = bills
}
