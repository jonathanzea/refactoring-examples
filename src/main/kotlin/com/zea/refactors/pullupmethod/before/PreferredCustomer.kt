package com.zea.refactors.pullupmethod.before

import java.time.LocalDate
import java.time.temporal.ChronoUnit

class PreferredCustomer(lastBillDate: LocalDate) : Customer(lastBillDate) {

    fun createBill(date: LocalDate) {
        val amount = chargeFor(lastBillDate, date)
        addBill(date, amount)
    }

    private fun chargeFor(startDate: LocalDate, endDate: LocalDate): Double {
        return if (startDate.until(endDate, ChronoUnit.DAYS) >= 10) {
            100.0
        } else {
            70.0
        }
    }
}
