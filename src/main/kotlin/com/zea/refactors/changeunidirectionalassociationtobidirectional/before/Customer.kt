package com.zea.refactors.changeunidirectionalassociationtobidirectional.before

data class Customer(val name: String)
