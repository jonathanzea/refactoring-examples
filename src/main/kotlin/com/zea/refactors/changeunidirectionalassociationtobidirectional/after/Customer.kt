package com.zea.refactors.changeunidirectionalassociationtobidirectional.after

data class Customer(val name: String) {

    val orders: MutableSet<Order> = mutableSetOf()

    fun putOrder(order: Order) {
        orders.add(order)
    }
}
