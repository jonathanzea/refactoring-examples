package com.zea.refactors.changeunidirectionalassociationtobidirectional.after

data class Order(private val productName: String, var customer: Customer) {
    init {
        this.customer.putOrder(this)
    }

    fun modifyCustomer(customer: Customer) {
        this.customer.orders.remove(this)
        this.customer = customer
    }
}
