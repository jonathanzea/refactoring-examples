package com.zea.refactors.replaceconstructorwithfactorymethod.after

class Male private constructor() {
    val genderCode = "M"

    companion object {
        fun createMale(): Male {
            return Male()
        }
    }
}
