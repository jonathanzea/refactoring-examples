package com.zea.refactors.replaceconstructorwithfactorymethod.after

class Female private constructor() {
    val genderCode = "F"

    companion object {
        fun createFemale(): Female {
            return Female()
        }
    }
}
