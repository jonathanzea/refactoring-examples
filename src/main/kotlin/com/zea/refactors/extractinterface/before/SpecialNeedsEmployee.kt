package com.zea.refactors.extractinterface.before

class SpecialNeedsEmployee {
    fun discountRate(): Double {
        return 0.2
    }

    fun hasSpecialNeeds(): Boolean {
        return true
    }
}
