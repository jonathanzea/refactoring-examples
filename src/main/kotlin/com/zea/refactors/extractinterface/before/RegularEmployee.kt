package com.zea.refactors.extractinterface.before

class RegularEmployee {
    fun discountRate(): Double {
        return 0.5
    }

    fun hasSpecialNeeds(): Boolean {
        return false
    }
}
