package com.zea.refactors.extractinterface.before

class ItemCalculator(val item: String, val price: Double) {

    fun calculateForRegularEmployee(employee: RegularEmployee): Double {
        val discountedPrice = price - (price * employee.discountRate())
        if (employee.hasSpecialNeeds()) {
            return discountedPrice - 5.0
        }
        return discountedPrice
    }

    fun calculateForSpecialNeedsEmployee(employee: SpecialNeedsEmployee): Double {
        val discountedPrice = price - (price * employee.discountRate())
        if (employee.hasSpecialNeeds()) {
            return discountedPrice - 5.0
        }
        return discountedPrice
    }
}
