package com.zea.refactors.extractinterface.after

class RegularEmployee : Billable {
    override fun discountRate(): Double {
        return 0.5
    }

    override fun hasSpecialNeeds(): Boolean {
        return false
    }
}
