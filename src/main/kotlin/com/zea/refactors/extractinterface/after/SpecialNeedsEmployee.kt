package com.zea.refactors.extractinterface.after

class SpecialNeedsEmployee : Billable {
    override fun discountRate(): Double {
        return 0.2
    }

    override fun hasSpecialNeeds(): Boolean {
        return true
    }
}
