package com.zea.refactors.extractinterface.after

interface Billable {
    fun discountRate(): Double
    fun hasSpecialNeeds(): Boolean
}
