package com.zea.refactors.extractinterface.after

class ItemCalculator(val item: String, val price: Double) {

    fun calculateFor(employee: Billable): Double {
        val discountedPrice = price - (price * employee.discountRate())
        if (employee.hasSpecialNeeds()) {
            return discountedPrice - 5.0
        }
        return discountedPrice
    }
}
