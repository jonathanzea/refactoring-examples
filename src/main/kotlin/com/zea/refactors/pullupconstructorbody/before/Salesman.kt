package com.zea.refactors.pullupconstructorbody.before

class Salesman : Employee {
    private val commissionPerSale: Double

    constructor(name: String, id: Int, commissionPerSale: Double) : super(name, id) {
        this.commissionPerSale = commissionPerSale
        if (isPrivileged()) assignCar("VW 1961")
    }

    fun printSummary() = "Name: $name - Id: $id - Commission: $commissionPerSale - Vehicle: $vehicle"

    override fun isPrivileged() = commissionPerSale > 0.7
}
