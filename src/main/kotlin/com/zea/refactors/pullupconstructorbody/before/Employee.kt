package com.zea.refactors.pullupconstructorbody.before

abstract class Employee {
    protected var vehicle = "none"
    protected val name: String
    protected val id: Int

    constructor(name: String, id: Int) {
        this.name = name
        this.id = id
    }

    protected fun assignCar(type: String) {
        vehicle = type
    }

    protected abstract fun isPrivileged(): Boolean
}
