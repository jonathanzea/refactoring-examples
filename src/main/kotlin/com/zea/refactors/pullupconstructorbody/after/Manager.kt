package com.zea.refactors.pullupconstructorbody.after

class Manager : Employee {
    private val grade: String

    constructor(name: String, id: Int, grade: String) : super(name, id) {
        this.grade = grade
        initializeForCarAssignature("Cherokee Classic 1991")
    }

    override fun isPrivileged() = grade == "MAJOR"

    fun printSummary() = "Name: $name - Id: $id - Grade: $grade - Vehicle: $vehicle"
}
