package com.zea.refactors.pullupconstructorbody.after

abstract class Employee {
    protected var vehicle = "none"
    protected val name: String
    protected val id: Int

    constructor(name: String, id: Int) {
        this.name = name
        this.id = id
    }

    private fun assignCar(type: String) {
        vehicle = type
    }

    protected fun initializeForCarAssignature(car: String) {
        if (isPrivileged()) assignCar(car)
    }

    protected abstract fun isPrivileged(): Boolean
}
