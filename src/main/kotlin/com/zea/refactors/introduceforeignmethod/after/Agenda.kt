package com.zea.refactors.introduceforeignmethod.after

import java.time.LocalDate

class Agenda {

    fun tomorrowDate() = todayDate().plusDays(1)!!
    private fun todayDate() = LocalDate.now()
}
