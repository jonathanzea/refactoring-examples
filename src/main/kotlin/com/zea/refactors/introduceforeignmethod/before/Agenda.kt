package com.zea.refactors.introduceforeignmethod.before

import java.time.LocalDate

class Agenda {

    fun todaysDate() = LocalDate.now()
}
