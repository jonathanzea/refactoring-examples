package com.zea.refactors.introduceparameterobject.after

import java.time.LocalDate

class DateRange(private val from: LocalDate, private val to: LocalDate) {
    fun includes(date: LocalDate) = date in from..to
}
