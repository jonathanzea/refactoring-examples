package com.zea.refactors.introduceparameterobject.after

import java.time.LocalDate

class Reservation(val reservationDate: LocalDate)
