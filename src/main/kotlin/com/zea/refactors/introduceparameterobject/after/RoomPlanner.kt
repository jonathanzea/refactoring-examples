package com.zea.refactors.introduceparameterobject.after

class RoomPlanner(private val reservations: List<Reservation>) {
    private var count = 0

    fun countReservationsInPeriod(dateRange: DateRange): Int {
        for (reservation in reservations) {
            if (dateRange.includes(reservation.reservationDate)) {
                count++
            }
        }
        return count
    }
}
