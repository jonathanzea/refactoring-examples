package com.zea.refactors.introduceparameterobject.before

import java.time.LocalDate

class RoomPlanner(private val reservations: List<Reservation>) {
    private var count = 0

    fun countReservationsInPeriod(from: LocalDate, to: LocalDate): Int {
        for (reservation in reservations) {
            if (reservation.reservationDate >= from && reservation.reservationDate <= to) {
                count++
            }
        }
        return count
    }
}
