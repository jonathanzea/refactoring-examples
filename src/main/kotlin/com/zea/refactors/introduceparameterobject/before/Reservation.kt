package com.zea.refactors.introduceparameterobject.before

import java.time.LocalDate

class Reservation(val reservationDate: LocalDate)
