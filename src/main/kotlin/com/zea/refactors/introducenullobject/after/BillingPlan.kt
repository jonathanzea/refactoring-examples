package com.zea.refactors.introducenullobject.after

enum class BillingPlan {
    ADVANCED,
    BASIC
}
