package com.zea.refactors.introducenullobject.after

open class Customer(val name: String, val paymentHistory: PaymentHistory, val billingPlan: BillingPlan)
