package com.zea.refactors.introducenullobject.after

class CustomerSummary(private val site: Site) {

    fun print(): String =
        "Customer (Type: ${site.customer.billingPlan}) - ${site.customer.name} - (Delinquency: ${site.customer.paymentHistory.weeksDelinquent})"
}
