package com.zea.refactors.introducenullobject.after

class Site(customer: Customer?) {

    val customer: Customer

    init {
        if (customer == null)
            this.customer = NullCustomer()
        else
            this.customer = customer
    }
}
