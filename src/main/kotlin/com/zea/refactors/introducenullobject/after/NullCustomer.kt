package com.zea.refactors.introducenullobject.after

class NullCustomer : Customer(
    "*absent*",
    NullPaymentHistory(),
    BillingPlan.BASIC
)
