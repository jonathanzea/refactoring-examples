package com.zea.refactors.introducenullobject.before

class Customer(val name: String, val paymentHistory: PaymentHistory, val billingPlan: BillingPlan)
