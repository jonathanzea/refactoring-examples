package com.zea.refactors.introducenullobject.before

enum class BillingPlan {
    ADVANCED,
    BASIC
}
