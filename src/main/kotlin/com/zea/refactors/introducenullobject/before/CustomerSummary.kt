package com.zea.refactors.introducenullobject.before

class CustomerSummary(val site: Site) {

    fun print(): String {
        val customer = site.customer

        var billingPlan: BillingPlan
        if (customer == null) {
            billingPlan = BillingPlan.BASIC
        } else {
            billingPlan = customer.billingPlan
        }

        var customerName: String
        if (customer == null) {
            customerName = "*absent*"
        } else {
            customerName = customer.name
        }

        var weeksDeliquent: Int
        if (customer == null) {
            weeksDeliquent = 0
        } else {
            weeksDeliquent = customer.paymentHistory.weeksDelinquent
        }

        return "Customer (Type: $billingPlan) - $customerName - (Delinquency: $weeksDeliquent)"
    }
}
