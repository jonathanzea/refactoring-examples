package com.zea.movierental.after

class RegularPrice : Price {

    override fun getPriceCode(): Int {
        return Movie.REGULAR
    }

    override fun getCharge(daysRented: Int): Double {
        var result = 2.0
        if (daysRented > 2) {
            result += (daysRented - 2) * 1.5
        }
        return result
    }

    override fun getFrequenterPoints(daysRented: Int): Int {
        return 1
    }
}
