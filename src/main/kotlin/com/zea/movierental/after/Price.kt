package com.zea.movierental.after

interface Price {
    fun getPriceCode(): Int
    fun getCharge(daysRented: Int): Double
    fun getFrequenterPoints(daysRented: Int): Int
}
