package com.zea.movierental.after

data class Customer(val name: String, val rentals: List<Rental>) {

    fun asciiStatement(): String {
        var result = "Rental Record for $name\n"
        rentals.forEach { rental ->
            result += "\t${rental.movie.title}\t${rental.getCharge()}\n"
        }
        result += "Amount owed is ${getTotalFrequenterPoints()}\n"
        result += "You earned ${getTotalCharge()} frequent renter points"
        return result
    }

    fun htmlStatement(): String {
        var result = "<H1>Rentals for <EM>$name</EM></H1><P>\n"
        rentals.forEach { rental ->
            result += "${rental.movie.title}: ${rental.getCharge()}<BR>\n"
        }
        result += "<P>You owe <EM>${getTotalFrequenterPoints()}</EM><P>\n"
        result += "On this rental you earned <EM>${getTotalCharge()}</EM> frequent renter points<P>"
        return result
    }

    private fun getTotalCharge(): Int {
        return rentals.map { it.getFrequenterPoints() }.sum()
    }

    private fun getTotalFrequenterPoints(): Double {
        return rentals.map { it.getCharge() }.sum()
    }
}
