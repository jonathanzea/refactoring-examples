package com.zea.movierental.after

data class Movie(val title: String, val priceCode: Int) {

    private val price: Price

    init {
        price = when (priceCode) {
            REGULAR -> RegularPrice()
            NEW_RELEASE -> NewReleasePrice()
            CHILDREN -> ChildrenPrice()
            else -> throw IllegalArgumentException("Incorrect Price Code: $priceCode")
        }
    }

    companion object {
        const val CHILDREN = 2
        const val REGULAR = 0
        const val NEW_RELEASE = 1
    }

    fun getCharge(daysRented: Int): Double {
        return price.getCharge(daysRented)
    }

    fun getFrequenterPoints(daysRented: Int): Int {
        return price.getFrequenterPoints(daysRented)
    }
}
