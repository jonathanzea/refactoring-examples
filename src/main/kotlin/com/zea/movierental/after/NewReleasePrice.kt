package com.zea.movierental.after

class NewReleasePrice : Price {

    override fun getPriceCode(): Int {
        return Movie.NEW_RELEASE
    }

    override fun getCharge(daysRented: Int): Double {
        return daysRented * 3.toDouble()
    }

    override fun getFrequenterPoints(daysRented: Int): Int {
        if (daysRented > 1) {
            return 2
        }
        return 1
    }
}
