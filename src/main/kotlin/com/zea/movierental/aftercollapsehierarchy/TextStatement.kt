package com.zea.movierental.aftercollapsehierarchy

class TextStatement : Statement() {

    override fun generateRentalEntryFor(rental: Rental) =
        "\t${rental.movie.title}\t${rental.getCharge()}\n"

    override fun generateHeaderFrom(customer: Customer) =
        "Rental Record for ${customer.name}\n"

    override fun footerFrom(customer: Customer): String {
        return "Amount owed is ${customer.getTotalFrequenterPoints()}\nYou earned ${customer.getTotalCharge()} frequent renter points"
    }
}
