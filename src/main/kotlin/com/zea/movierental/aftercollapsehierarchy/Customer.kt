package com.zea.movierental.aftercollapsehierarchy

data class Customer(val name: String, val rentals: List<Rental>) {

    fun asciiStatement(): String {
        return TextStatement().value(this)
    }

    fun htmlStatement(): String {
        return HTMLStatement().value(this)
    }

    internal fun getTotalCharge(): Int {
        return rentals.map { it.getFrequenterPoints() }.sum()
    }

    internal fun getTotalFrequenterPoints(): Double {
        return rentals.map { it.getCharge() }.sum()
    }
}
