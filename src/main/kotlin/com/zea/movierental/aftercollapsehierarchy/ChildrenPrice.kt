package com.zea.movierental.aftercollapsehierarchy

class ChildrenPrice : Price {

    override fun getPriceCode(): Int {
        return Movie.CHILDREN
    }

    override fun getCharge(daysRented: Int): Double {
        var result = 1.5
        if (daysRented > 3) {
            result += (daysRented - 3) * 1.5
        }
        return result
    }

    override fun getFrequenterPoints(daysRented: Int): Int {
        return 1
    }
}
