package com.zea.movierental.aftercollapsehierarchy

class HTMLStatement : Statement() {

    override fun generateRentalEntryFor(rental: Rental) =
        "${rental.movie.title}: ${rental.getCharge()}<BR>\n"

    override fun generateHeaderFrom(customer: Customer) =
        "<H1>Rentals for <EM>${customer.name}</EM></H1><P>\n"

    override fun footerFrom(customer: Customer): String {
        return "<P>You owe <EM>${customer.getTotalFrequenterPoints()}</EM><P>\nOn this rental you earned <EM>${customer.getTotalCharge()}</EM> frequent renter points<P>"
    }
}
