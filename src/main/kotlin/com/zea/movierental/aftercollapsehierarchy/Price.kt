package com.zea.movierental.aftercollapsehierarchy

interface Price {
    fun getPriceCode(): Int
    fun getCharge(daysRented: Int): Double
    fun getFrequenterPoints(daysRented: Int): Int
}
