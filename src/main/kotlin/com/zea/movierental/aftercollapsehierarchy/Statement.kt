package com.zea.movierental.aftercollapsehierarchy

abstract class Statement {

    fun value(customer: Customer): String {
        var result = generateHeaderFrom(customer)
        customer.rentals.forEach { rental ->
            result += generateRentalEntryFor(rental)
        }
        result += footerFrom(customer)
        return result
    }

    abstract fun generateRentalEntryFor(rental: Rental): String
    abstract fun generateHeaderFrom(customer: Customer): String
    abstract fun footerFrom(customer: Customer): String
}
