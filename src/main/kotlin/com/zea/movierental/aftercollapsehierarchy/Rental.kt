package com.zea.movierental.aftercollapsehierarchy

data class Rental(val movie: Movie, val daysRented: Int) {

    fun getCharge(): Double {
        return movie.getCharge(daysRented)
    }

    fun getFrequenterPoints(): Int {
        return movie.getFrequenterPoints(daysRented)
    }
}
