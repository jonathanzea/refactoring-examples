package com.zea.movierental.before

data class Rental(val movie: Movie, val daysRented: Int)
