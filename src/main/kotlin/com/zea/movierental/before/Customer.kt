package com.zea.movierental.before

import com.zea.movierental.before.Movie.Companion.CHILDREN
import com.zea.movierental.before.Movie.Companion.NEW_RELEASE
import com.zea.movierental.before.Movie.Companion.REGULAR

data class Customer(val name: String, val rentals: List<Rental>) {

    fun statement(): String {
        var totalAmount = 0.0
        var frequentRenterPoints = 0
        var result = "Rental Record for $name\n"

        rentals.forEach {
            var amount: Double = 0.0

            when (it.movie.priceCode) {
                REGULAR -> {
                    amount += 2.0
                    if (it.daysRented > 2) {
                        amount += (it.daysRented - 2) * 1.5
                    }
                }
                NEW_RELEASE -> {
                    amount += (it.daysRented) * 3
                }
                CHILDREN -> {
                    amount += 1.5
                    if (it.daysRented > 3) {
                        amount += (it.daysRented - 3) * 1.5
                    }
                }
            }
            // add frequent renter points
            frequentRenterPoints++

            // add bonus for a two days release rental
            if (it.movie.priceCode == NEW_RELEASE && it.daysRented > 1
            ) {
                frequentRenterPoints++
            }

            // show figures for this rental
            result += "\t${it.movie.title}\t$amount\n"
            totalAmount += amount
        }

        // add footer lines
        result += "Amount owed is $totalAmount\n"
        result += "You earned $frequentRenterPoints frequent renter points"

        return result
    }
}
