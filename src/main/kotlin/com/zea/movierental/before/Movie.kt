package com.zea.movierental.before

data class Movie(val title: String, val priceCode: Int) {
    companion object {
        const val CHILDREN = 2
        const val REGULAR = 0
        const val NEW_RELEASE = 1
    }
}
