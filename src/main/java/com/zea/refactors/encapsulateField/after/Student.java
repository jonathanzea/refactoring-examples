package com.zea.refactors.encapsulateField.after;

public class Student {

    private String name;
    private int nameWasGotten;
    private int nameWasSet;

    public Student(final String name) {
        this.name = name;
        this.nameWasGotten = 0;
        this.nameWasSet = 0;
    }

    public int howManyTimesWasGotten(final String field) {
        if (field.equals("name")) {
            return nameWasGotten;
        }
        return 0;
    }

    public int howManyTimesWasSet(final String field) {
        if (field.equals("name")) {
            return nameWasSet;
        }
        return 0;
    }

    public String getName() {
        this.nameWasGotten++;
        return name;
    }

    public void setName(final String name) {
        this.nameWasSet++;
        this.name = name;
    }
}
