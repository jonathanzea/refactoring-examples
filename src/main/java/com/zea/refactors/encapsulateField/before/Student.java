package com.zea.refactors.encapsulateField.before;

public class Student {

    public String name;

    public Student(final String name) {
        this.name = name;
    }
}
