package com.zea.refactors.removeassignmentstoparameters;

public class BeforeRemoveAssignmentToParameters {

    public String calculateDiscountFor(int initialDiscount, int quantity, int yearsOfCustomerInContract) {
        if (initialDiscount > 50) initialDiscount += 10;
        if (quantity > 100) initialDiscount += 10;
        if (yearsOfCustomerInContract > 5) initialDiscount += 25;
        return "Customer discount € " + initialDiscount + ".00";
    }
}
