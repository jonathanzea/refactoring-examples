package com.zea.refactors.removeassignmentstoparameters;

public class BeforeSubstituteAlgorithm {

    private final String[] persons;

    public BeforeSubstituteAlgorithm(String... persons) {
        this.persons = persons;
    }

    public String find(final String name) {
        for (int i = 0; i< persons.length; i++){
            if (persons[i].equals(name)) {
                return "FOUND: Persons[" + i + "]=" + persons[i];
            }
        }
        return "NOT FOUND";
    }
}
