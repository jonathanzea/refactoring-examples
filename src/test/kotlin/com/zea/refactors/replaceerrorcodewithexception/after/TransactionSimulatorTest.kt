package com.zea.refactors.replaceerrorcodewithexception.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TransactionSimulatorTest {

    @Test
    fun `responds with OK message for positive simulation`() {
        val account = Account(12, 1500.5)
        val simulator = TransactionSimulator(account)
        assertEquals(simulator.simulateTransactionOf(1000.0), "SUCCESS - Transaction possible to do - Account (id: 12) new balance: €500.5")
    }

    @Test
    fun `responds with NOK message for positive simulation`() {
        val account = Account(8, 200.25)
        val simulator = TransactionSimulator(account)
        assertEquals(simulator.simulateTransactionOf(900.0), "Account (id: 8) has not sufficient funds")
    }
}
