package com.zea.refactors.replaceerrorcodewithexception.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class AccountTest {

    @Test
    fun `fails on overdrawn`() {
        val exception = assertThrows<OverdrawnException> {
            Account(1, 100.0).withdraw(200.0)
        }
        assertEquals("Amount too large", exception.message)
    }

    @Test
    fun `can withdraw`() {
        val account = Account(2, 200.0)
        account.withdraw(100.0)
        assertEquals(100.0, account.balance, 100.0)
    }
}
