package com.zea.refactors.introduceforeignmethod.before

import java.time.LocalDate.now
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AgendaTest {

    @Test
    fun `agenda can generate today and tomorrow date`() {
        val agenda = Agenda()
        assertEquals(now().plusDays(1), agenda.todaysDate().plusDays(1))
    }
}
