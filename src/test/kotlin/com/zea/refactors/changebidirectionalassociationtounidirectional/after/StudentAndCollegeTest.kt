package com.zea.refactors.changebidirectionalassociationtounidirectional.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StudentAndCollegeTest {

    @Test
    fun `college can be compared by structure and by reference`() {
        val college = College("UCV")
        assertEquals(College("UCV"), college)
        assertEquals(college, college)
    }

    @Test
    fun `student can be compared by structure and by reference`() {
        val student = Student(
            "Igor Kasparov",
            College("MIT")
        )
        assertEquals(
            Student(
                "Igor Kasparov",
                College("MIT")
            ), student)
        assertEquals(student, student)
    }

    @Test
    fun `student its created with a college`() {
        val college = College("Harvard")
        val student =
            Student("Jonathan Zea", college)
        assertEquals(College("Harvard"), student.college)
    }

    @Test
    fun `student can switch colleges`() {
        val college = College("Standford")
        val student =
            Student("Valery McKain", college)
        student.switchCollege(College("MIT"))
        assertEquals(student.college,
            College("MIT")
        )
    }

    @Test
    fun `college expose the nationality of its students`() {
        val college = College("USB")
        val student =
            Student("Hilary Swamp", college)

        assertEquals("American", college.getStudentNationality(student))
    }
}
