package com.zea.refactors.changebidirectionalassociationtounidirectional.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StudentAndCollegeTest {

    @Test
    fun `college can be compared by structure and by reference`() {
        val college = College("UCV")
        assertEquals(College("UCV"), college)
        assertEquals(college, college)
    }

    @Test
    fun `student can be compared by structure and by reference`() {
        val student = Student(
            "Igor Kasparov",
            College("MIT")
        )
        assertEquals(
            Student(
                "Igor Kasparov",
                College("MIT")
            ), student
        )
        assertEquals(student, student)
    }

    @Test
    fun `student its created with a college`() {
        val college = College("Harvard")
        val student =
            Student("Jonathan Zea", college)
        assertEquals(College("Harvard"), student.college)
    }

    @Test
    fun `college holds the reference to the student which was created with`() {
        val college = College("Princeton")
        val student1 =
            Student("Hector Zea", college)
        val student2 =
            Student("Stuart Wickers", college)

        assertEquals(2, college.students.size)
        assertEquals(setOf(student1, student2), college.students)
    }

    @Test
    fun `student can switch colleges`() {
        val college = College("Standford")
        val student =
            Student("Valery McKain", college)

        assertEquals(1, college.students.size)
        student.switchCollege(College("MIT"))

        assertEquals(0, college.students.size)
    }

    @Test
    fun `college expose the nationality of its students`() {
        val college = College("USB")
        val student =
            Student("Hilary Swamp", college)

        assertEquals("American", college.getStudentNationality(student))
    }
}
