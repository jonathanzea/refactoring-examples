package com.zea.refactors.consolidateduplicateconditionalfragments.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MealDealTest {

    @Test
    fun `a meal from the deal menu returns deal price`() {
        val meal = Meal("Meat Balls")
        assertEquals("Meat Balls - x1 - €7.0", meal.getBill())
    }

    @Test
    fun `a meal outside the deal menu returns normal price`() {
        val meal = Meal("Beef Dumplings")
        assertEquals("Beef Dumplings - x1 - €13.4", meal.getBill())
    }
}
