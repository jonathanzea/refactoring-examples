package com.zea.refactors.changereferencetovalue.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

class CurrencyTest {

    @Test
    fun `currency reference can be mutated`() {
        val usd = Currency.getExchangeRate("USD")
        assertEquals(1.1, usd.exchangeRate)
    }

    @Test
    fun `two structural equal currencies are still different`() {
        assertNotEquals(Currency.getExchangeRate("USD"), Currency.getExchangeRate("USD"))
    }
}
