package com.zea.refactors.changereferencetovalue.after

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CurrencyTest {

    @Test
    fun `currency is immutable`() {
        val usd = Currency("USD", 1.1)
        Assertions.assertEquals(1.1, usd.exchangeRate)
    }

    @Test
    fun `two structural equal currencies are equal`() {
        Assertions.assertEquals(
            Currency(
                "BSF",
                3000000.23
            ), Currency("BSF", 3000000.23)
        )
    }
}
