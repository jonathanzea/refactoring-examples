package com.zea.refactors.replaceexceptionwithtest.before

class GunClip(var ammunition: Int) {
    fun pop() {
        ammunition--
    }
}
