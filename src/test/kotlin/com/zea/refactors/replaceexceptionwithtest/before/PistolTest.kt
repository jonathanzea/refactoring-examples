package com.zea.refactors.replaceexceptionwithtest.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PistolTest {

    @Test
    fun `reloads on clip with bullets`() {
        val clip = GunClip(3)
        val pistol = Gun(clip)
        pistol.reload()
        assertEquals(2, pistol.ammunitionInClip())
        assertEquals(0, pistol.clipsUsed())
    }

    @Test
    fun `fails on reloading with empty clip`() {
        val clip = GunClip(0)
        val pistol = Gun(clip)
        pistol.reload()
        assertEquals(3, pistol.ammunitionInClip())
        assertEquals(1, pistol.clipsUsed())
    }
}
