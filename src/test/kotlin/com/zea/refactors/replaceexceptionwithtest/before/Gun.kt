package com.zea.refactors.replaceexceptionwithtest.before

class Gun(private var gunClip: GunClip) {

    private var clipsUsed = 0

    fun reload() = if (gunClip.ammunition > 0) gunClip.pop() else discardClipAndReload()

    fun clipsUsed() = clipsUsed

    fun ammunitionInClip(): Int = gunClip.ammunition

    private fun discardClipAndReload() {
        this.gunClip = GunClip(3)
        clipsUsed++
    }
}
