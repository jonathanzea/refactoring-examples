package com.zea.refactors.replaceexceptionwithtest.after

class GunClip(var ammunition: Int) {

    fun pop() {
        if (ammunition == 0) {
            throw NoAmmunitionException()
        }
        ammunition--
    }
}
