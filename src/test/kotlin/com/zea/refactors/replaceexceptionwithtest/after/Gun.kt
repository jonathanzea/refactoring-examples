package com.zea.refactors.replaceexceptionwithtest.after

class Gun(private var gunClip: GunClip) {

    private var clipsUsed = 0

    fun reload() {
        try {
            gunClip.pop()
        } catch (exception: NoAmmunitionException) {
            this.gunClip = GunClip(3)
            clipsUsed++
        }
    }

    fun clipsUsed() = clipsUsed

    fun ammunitionInClip(): Int = gunClip.ammunition
}
