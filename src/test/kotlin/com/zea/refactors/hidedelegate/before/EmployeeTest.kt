package com.zea.refactors.hidedelegate.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `exposes the manager on charge of its department`() {
        val employee = Employee(
            "Belkys",
            Department("Preschool")
        )
        assertEquals("Iris", employee.department.getManager())
    }
}
