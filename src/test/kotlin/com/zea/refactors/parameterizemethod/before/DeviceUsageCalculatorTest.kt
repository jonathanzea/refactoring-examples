package com.zea.refactors.parameterizemethod.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DeviceUsageCalculatorTest {

    @Test
    fun `calculates amount for high usage`() {
        val deviceUsageCalculator = DeviceUsageCalculator(800)
        assertEquals(391.0, deviceUsageCalculator.calculate(), "Device usage price calculation does not match")
    }

    @Test
    fun `calculates amount for medium usage`() {
        val deviceUsageCalculator = DeviceUsageCalculator(150)
        assertEquals(148.0, deviceUsageCalculator.calculate(), "Device usage price calculation does not match")
    }

    @Test
    fun `calculates amount for low usage`() {
        val deviceUsageCalculator = DeviceUsageCalculator(50)
        assertEquals(1.5, deviceUsageCalculator.calculate(), "Device usage price calculation does not match")
    }
}
