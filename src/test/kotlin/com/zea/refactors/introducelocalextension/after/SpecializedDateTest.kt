package com.zea.refactors.introducelocalextension.after

import java.util.Date
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SpecializedDateTest {

    @Test
    fun `specialized date service return tomorrows day code using`() {
        val specializedDate = SpecializedDate()
        assertEquals(Date().day + 1, specializedDate.tomorrowsDayCode())
    }

    @Test
    fun `specialized date service return yesterdays day code using`() {
        val specializedDate = SpecializedDate()
        assertEquals(Date().day - 1, specializedDate.yesterdaysDayCode())
    }
}
