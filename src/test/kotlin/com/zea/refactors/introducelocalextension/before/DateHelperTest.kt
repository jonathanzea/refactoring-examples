package com.zea.refactors.introducelocalextension.before

import java.util.Date
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DateHelperTest {

    @Test
    fun `specialized date service return tomorrows day code using`() {
        val specializedDate = DateHelper(Date())
        Assertions.assertEquals(Date().day + 1, specializedDate.tomorrowsDayCode())
    }

    @Test
    fun `specialized date service return yesterdays day code using`() {
        val specializedDate = DateHelper(Date())
        Assertions.assertEquals(Date().day - 1, specializedDate.yesterdaysDayCode())
    }
}
