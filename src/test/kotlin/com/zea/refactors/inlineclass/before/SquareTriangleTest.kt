package com.zea.refactors.inlineclass.before

import kotlin.math.sqrt
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SquareTriangleTest {

    @Test
    fun`calculates hypotenuse by pythagoras theorem`() {
        val squareTriangle = SquareTriangle(3.0, 7.0, Pythagoras())
        assertEquals(sqrt(58.0), squareTriangle.getHypotenuse())
    }
}
