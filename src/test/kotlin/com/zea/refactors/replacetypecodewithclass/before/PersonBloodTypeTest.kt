package com.zea.refactors.replacetypecodewithclass.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PersonBloodTypeTest {

    @Test
    fun `prints patient summary info`() {
        val patientO = Patient(
            "Valery",
            Patient.O
        )
        assertEquals("Valery - Blood Type: 0", patientO.printPatientInfo())

        val patientA = Patient(
            "Valery",
            Patient.A
        )
        assertEquals("Valery - Blood Type: 1", patientA.printPatientInfo())

        val patientB = Patient(
            "Valery",
            Patient.B
        )
        assertEquals("Valery - Blood Type: 2", patientB.printPatientInfo())

        val patientAB = Patient(
            "Valery",
            Patient.AB
        )
        assertEquals("Valery - Blood Type: 3", patientAB.printPatientInfo())
    }
}
