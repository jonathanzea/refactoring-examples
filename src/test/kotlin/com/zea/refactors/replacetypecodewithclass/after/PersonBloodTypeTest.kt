package com.zea.refactors.replacetypecodewithclass.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PersonBloodTypeTest {

    @Test
    fun `prints patient summary info`() {
        val patientO = Patient(
            "Valery",
            BloodGroup.O.code
        )
        assertEquals("Valery - Blood Type: 0", patientO.printPatientInfo())

        val patientA = Patient(
            "Valery",
            BloodGroup.A.code
        )
        assertEquals("Valery - Blood Type: 1", patientA.printPatientInfo())

        val patientB = Patient(
            "Valery",
            BloodGroup.B.code
        )
        assertEquals("Valery - Blood Type: 2", patientB.printPatientInfo())

        val patientAB = Patient(
            "Valery",
            BloodGroup.AB.code
        )
        assertEquals("Valery - Blood Type: 3", patientAB.printPatientInfo())
    }
}
