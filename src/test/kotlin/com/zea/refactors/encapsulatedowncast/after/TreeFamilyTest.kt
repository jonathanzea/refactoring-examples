package com.zea.refactors.encapsulatedowncast.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TreeFamilyTest {

    @Test
    fun `returns the last node of the list as the parent type`() {
        val treeFamily = TreeFamily(
            listOf(
                BluePine(),
                BluePine(),
                BluePine()
            )
        )
        assertEquals(treeFamily.getLastPineParentSpecie() is Pine, true)
    }
}
