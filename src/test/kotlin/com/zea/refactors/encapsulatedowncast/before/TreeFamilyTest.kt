package com.zea.refactors.encapsulatedowncast.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TreeFamilyTest {

    @Test
    fun `returns the last node of the list as the parent type`() {
        val treeFamily = TreeFamily(
            listOf(
                BluePine(),
                BluePine(),
                BluePine()
            )
        )
        val pine = treeFamily.getLastPineParentSpecie() as Pine
        assertEquals(pine is Pine, true)
    }
}
