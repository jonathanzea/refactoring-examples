package com.zea.refactors.consolidateconditionalexpression.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CustomerTicketPriceTest {

    @Test
    fun `low price configured customer returns low price`() {
        val customer = Customer(true, 12, "student")
        assertEquals(10.00, customer.getTicketPrice())
    }

    @Test
    fun `high price configured customer returns high price`() {
        val customer = Customer(false, 13, "employee")
        assertEquals(25.00, customer.getTicketPrice())
    }
}
