package com.zea.refactors.changevaluetoreference.after

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class OrderServiceTest {

    @Test
    fun `hold the order customers references and doesn't repeat if it's the same customer`() {
        val customer = Customer.create("IBM Services S.A.")
        customer.addOrder()

        val customerCatalog = OrderService(setOf(customer, customer, customer))
        Assertions.assertEquals(1, customerCatalog.numberOfOrdersFor("IBM Services S.A."))
        customer.addOrder()
        customer.addOrder()
        Assertions.assertEquals(3, customerCatalog.numberOfOrdersFor("IBM Services S.A."))
    }
}
