package com.zea.refactors.changevaluetoreference.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

class CustomerTest {

    @Test
    fun `compares two customers`() {
        assertEquals(Customer.create("Vance Refrigeration Ghmb"), Customer.create("Vance Refrigeration Ghmb"))
        assertNotEquals(Customer.create("Standards & Poors S.A."), Customer.create("Pollos Hermanos C.A."))
    }

    @Test
    fun `increments the order numbers and gives the quantity`() {
        val customer = Customer.create("Ernst & Young IT Services Smp")
        assertEquals(0, customer.getNumberOfOrders())
        customer.addOrder()
        customer.addOrder()
        assertEquals(2, customer.getNumberOfOrders())
    }
}
