package com.zea.refactors.changevaluetoreference.before

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class OrderServiceTest {

    @Test
    fun `hold the order customers references`() {
        val customer = Customer("IBM Services S.A.")
        customer.addOrder()
        val customerCatalog = OrderService(listOf(customer, customer, customer))
        Assertions.assertEquals(3, customerCatalog.numberOfOrdersFor("IBM Services S.A."))
    }
}
