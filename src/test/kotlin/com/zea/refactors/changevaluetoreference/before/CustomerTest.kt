package com.zea.refactors.changevaluetoreference.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

class CustomerTest {

    @Test
    fun `compares two customers`() {
        assertNotEquals(Customer("Standards & Poors S.A."), Customer("Standards & Poors S.A."))
    }

    @Test
    fun `increments the order numbers and gives the quantity`() {
        val customer = Customer("Ernst & Young IT Services Smp")
        assertEquals(0, customer.getNumberOfOrders())
        customer.addOrder()
        customer.addOrder()
        assertEquals(2, customer.getNumberOfOrders())
    }
}
