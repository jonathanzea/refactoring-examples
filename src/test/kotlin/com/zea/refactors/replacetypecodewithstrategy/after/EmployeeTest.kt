package com.zea.refactors.replacetypecodewithstrategy.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `pay engineer salary`() {
        val engineer = Employee(
            1000.0,
            0.0,
            0.0,
            Engineer()
        )
        assertEquals(engineer.paySalary(), 1000.0)
    }

    @Test
    fun `pay manager salary`() {
        val manager = Employee(
            500.0,
            500.0,
            0.0,
            Manager()
        )
        assertEquals(manager.paySalary(), 1000.0)
    }

    @Test
    fun `pay salesman salary`() {
        val salesman = Employee(
            250.0,
            0.0,
            200.0,
            Salesman()
        )
        assertEquals(salesman.paySalary(), 450.0)
    }
}
