package com.zea.refactors.replacetypecodewithstrategy.before

import com.zea.refactors.replacetypecodewithstrategy.before.Employee.Companion.ENGINEER
import com.zea.refactors.replacetypecodewithstrategy.before.Employee.Companion.MANAGER
import com.zea.refactors.replacetypecodewithstrategy.before.Employee.Companion.SALESMAN
import java.lang.IllegalArgumentException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class EmployeeTest {

    @Test
    fun `pay engineer salary`() {
        val engineer = Employee(ENGINEER, 1000.0, 0.0, 0.0)
        assertEquals(engineer.paySalary(), 1000.0)
    }

    @Test
    fun `pay manager salary`() {
        val manager = Employee(MANAGER, 500.0, 500.0, 0.0)
        assertEquals(manager.paySalary(), 1000.0)
    }

    @Test
    fun `pay salesman salary`() {
        val salesman = Employee(SALESMAN, 250.0, 0.0, 200.0)
        assertEquals(salesman.paySalary(), 450.0)
    }

    @Test
    fun `fails on unrecognized employee code`() {
        val exception = assertThrows<IllegalArgumentException>
        { Employee(50, 0.0, 0.0, 0.0).paySalary() }
        assertEquals("Unknown employee code: 50", exception.message)
    }
}
