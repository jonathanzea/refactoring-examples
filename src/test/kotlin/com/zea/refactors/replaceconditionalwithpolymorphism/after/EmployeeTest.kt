package com.zea.refactors.replaceconditionalwithpolymorphism.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `calculates the salary for engineer type`() {
        val employee =
            Employee(Engineer())
        assertEquals(50000.0, employee.getSalary())
    }

    @Test
    fun `calculates the salary for salesman type`() {
        val employee =
            Employee(Salesman())
        assertEquals(20000.0, employee.getSalary())
    }

    @Test
    fun `calculates the salary for manager type`() {
        val employee =
            Employee(Manager())
        assertEquals(80000.0, employee.getSalary())
    }
}
