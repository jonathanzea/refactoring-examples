package com.zea.refactors.replaceconditionalwithpolymorphism.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `calculates the salary for engineer type`() {
        val employee =
            Employee(EmployeeType.ENGINEER)
        assertEquals(50000.0, employee.getSalary())
    }

    @Test
    fun `calculates the salary for salesman type`() {
        val employee =
            Employee(EmployeeType.SALESMAN)
        assertEquals(20000.0, employee.getSalary())
    }

    @Test
    fun `calculates the salary for manager type`() {
        val employee =
            Employee(EmployeeType.MANAGER)
        assertEquals(80000.0, employee.getSalary())
    }
}
