package com.zea.refactors.replacerecordwithdataclass.after

import java.lang.ClassCastException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class VehicleRegistryTest {

    @Test
    fun `prints the vehicle summary`() {
        val vehicleRegistry = VehicleRegistry(arrayOf("Thunder", 500000.0, "KFZ-2"))
        assertEquals("Summary = Thunder - 500000.0 - KFZ-2", vehicleRegistry.printVehicleSummary())
    }

    @Test
    fun `fails on string price passed`() {
        assertThrows<ClassCastException>
        { VehicleRegistry(arrayOf(10, "hallo", "KFZ-2")).printVehicleSummary() }
    }
}
