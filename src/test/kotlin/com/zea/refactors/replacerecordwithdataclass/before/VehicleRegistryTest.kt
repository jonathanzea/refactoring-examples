package com.zea.refactors.replacerecordwithdataclass.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class VehicleRegistryTest {

    @Test
    fun `prints the vehicle summary`() {
        val vehicle = Vehicle("Thunder", 500000.0, "KFZ-2")
        val vehicleRegistry = VehicleRegistry(vehicle)
        assertEquals("Summary = Thunder - 500000.0 - KFZ-2", vehicleRegistry.printVehicleSummary())
    }
}
