package com.zea.refactors.introducenullobject.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CustomerSummaryTest {

    @Test
    fun `prints the site customer summary`() {
        val customerSummary = CustomerSummary(
            Site(
                Customer(
                    "Jonathan Zea",
                    PaymentHistory(3),
                    BillingPlan.ADVANCED
                )
            )
        )
        assertEquals("Customer (Type: ADVANCED) - Jonathan Zea - (Delinquency: 3)", customerSummary.print())
    }

    @Test
    fun `prints the site customer summary on null customer`() {
        val customerSummary = CustomerSummary(
            Site(null)
        )
        assertEquals("Customer (Type: BASIC) - *absent* - (Delinquency: 0)", customerSummary.print())
    }
}
