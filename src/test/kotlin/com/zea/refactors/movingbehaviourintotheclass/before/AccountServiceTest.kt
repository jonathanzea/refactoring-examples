package com.zea.refactors.movingbehaviourintotheclass.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AccountServiceTest {

    @Test
    fun `can count the customer total products`() {
        val customer = BankCustomer()
        customer.addProduct(Product("LOAN", 40000.00))
        customer.addProduct(Product("LOAN", 10000.00))
        customer.addProduct(Product("ACCOUNT", 100000.00))

        val accountService = AccountService(customer)
        assertEquals(accountService.customerTotalProducts(), 3)
    }

    @Test
    fun `can give a total money amount of customer loans`() {
        val customer = BankCustomer()
        customer.addProduct(Product("LOAN", 40000.00))
        customer.addProduct(Product("LOAN", 10000.00))
        customer.addProduct(Product("ACCOUNT", 100000.00))

        val accountService = AccountService(customer)
        assertEquals(accountService.customerTotalLoansAmount(), 50000.00)
    }
}
