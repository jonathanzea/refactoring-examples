package com.zea.refactors.extractsuperclass.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestEmployeeAndDepartment {

    @Test
    fun `prints an employee summary`() {
        val employee = Employee("Maria", "A0399", 10000.0)
        assertEquals("Employee: Maria(A0399) - Annual Cost: 10000.0", employee.getSummary())
    }

    @Test
    fun `prints a department summary`() {
        val department = Department(
            "Cleaning", listOf(
                Employee("Maria", "A0399", 10000.0),
                Employee("Mabel", "A0339", 8000.0)
            )
        )
        assertEquals("Department: Cleaning - Staff: 2 people - Annual Cost: 18000.0", department.printSummary())
    }
}
