package com.zea.refactors.movefield.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AccountTest {

    @Test
    fun `calculates interests`() {
        val account = Account(AccountType("PREMIUM", 5.0))
        assertEquals("PREMIUM - interests: €12.328767123287673", account.interestsFor(1000.0, 90))
    }
}
