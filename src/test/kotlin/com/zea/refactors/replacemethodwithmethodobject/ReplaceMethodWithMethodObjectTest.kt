package com.zea.refactors.replacemethodwithmethodobject

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReplaceMethodWithMethodObjectTest {

    @Test
    fun `Before Refactor`() {
        val policyCenter = BeforeReplaceMethodWithMethodObject("PET_INSURANCE")
        assertEquals(
            "{\"insured_name\":\"Maria Virginia Fereda\"," +
                "\"payment_method\":\"CREDIT_CARD\"," +
                "\"instalments\":6," +
                "\"for\":\"PET_INSURANCE\"" +
                "\"instalment_amount\":45.0" +
                "}",
            policyCenter.printPolicySummary(
                "Maria Virginia Fereda",
                "CREDIT_CARD",
                6,
                45.0)
        )
    }

    @Test
    fun `After Refactor - Replace Method with Method Object`() {
        val policyCenter = AfterReplaceMethodWithMethodObject("HORSE_INSURANCE")
        assertEquals(
            "{\"insured_name\":\"Maria Virginia Fereda\"," +
                    "\"payment_method\":\"CREDIT_CARD\"," +
                    "\"instalments\":6," +
                    "\"for\":\"HORSE_INSURANCE\"" +
                    "\"instalment_amount\":45.0" +
                    "}",
            policyCenter.printPolicySummary(
                "Maria Virginia Fereda",
                "CREDIT_CARD",
                6,
                45.0)
        )
    }
}
