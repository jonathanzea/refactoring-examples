package com.zea.refactors.replaceinheritancewithdelegation.before

import java.util.Vector

class GitStash private constructor() : Vector<String>(1) {

    companion object {
        fun create(): GitStash {
            return GitStash()
        }
    }

    fun stash(content: String) {
        this.add(0, content)
    }

    fun pop(): String {
        return this.removeAt(0)
    }

    fun clean() {
        this.clear()
    }
}
