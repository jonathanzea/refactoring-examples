package com.zea.refactors.replaceinheritancewithdelegation.after

import java.util.Vector

class GitStash private constructor() {

    private val simpleVector: Vector<String> = Vector(1)

    companion object {
        fun create(): GitStash {
            return GitStash()
        }
    }

    fun size() = simpleVector.size
    fun stash(content: String) = simpleVector.add(0, content)
    fun pop(): String = simpleVector.removeAt(0)
    fun clean() = simpleVector.clear()
}
