package com.zea.refactors.replaceinheritancewithdelegation.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GitStashTest {

    @Test
    fun `stash content and pops it`() {
        val gitStash = GitStash.create()
        gitStash.stash("Commit 001 - SomeClass:19")
        assertEquals(1, gitStash.size())
        assertEquals("Commit 001 - SomeClass:19", gitStash.pop())
        assertEquals(0, gitStash.size())
    }

    @Test
    fun `stash content and cleans it`() {
        val gitStash = GitStash.create()
        gitStash.stash("Commit 001 - SomeClass:19")
        gitStash.clean()
        assertEquals(0, gitStash.size())
    }
}
