package com.zea.refactors.splittemporaryvariable

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class SplitTemporaryVariableTest {

    @Test
    fun `Before Refactor`() {
        val absolutePathBuilder = BeforeSplitTemporaryVariable(true, "policy.pdf")
        Assertions.assertEquals("/usr/bin/policy.pdf", absolutePathBuilder.createPath())

        val relativePathBuilder = BeforeSplitTemporaryVariable(false, "claim.pdf")
        Assertions.assertEquals("users/jonathan/bin/claim.pdf", relativePathBuilder.createPath())
    }

    @Test
    fun `Refactor After - Split temporary variable (temp)`() {
        val absolutePathBuilder = AfterSplitTemporaryVariable(true, "policy.pdf")
        Assertions.assertEquals("/usr/bin/policy.pdf", absolutePathBuilder.createPath())

        val relativePathBuilder = AfterSplitTemporaryVariable(false, "claim.pdf")
        Assertions.assertEquals("users/jonathan/bin/claim.pdf", relativePathBuilder.createPath())
    }
}
