package com.zea.refactors.extractclass.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PersonTest {

    @Test
    fun `prints a contact with phone number`() {
        val person = Person("Belkys", PhoneNumber("+54", "911", "1630201"))
        assertEquals("Belkys \nTelefonnummer: +54 - 911 - 1630201", person.printContact())
    }
}
