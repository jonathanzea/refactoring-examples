package com.zea.refactors.replacetypecodewithsubclasses.after

import com.zea.refactors.replacetypecodewithsubclasses.after.Employee.Companion.MANAGER
import com.zea.refactors.replacetypecodewithsubclasses.after.Employee.Companion.SALESMAN
import com.zea.refactors.replacetypecodewithsubclasses.before.Employee.Companion.ENGINEER
import java.lang.IllegalArgumentException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class EmployeeTest {

    @Test
    fun `creates an engineer`() {
        val employee = Employee.create(1)
        assertEquals(ENGINEER, employee.getType())
    }

    @Test
    fun `creates an salesman`() {
        val employee = Employee.create(2)
        assertEquals(SALESMAN, employee.getType())
    }

    @Test
    fun `creates an manager`() {
        val employee = Employee.create(3)
        assertEquals(MANAGER, employee.getType())
    }

    @Test
    fun `throws exception on unknown carrer code`() {
        val exception = assertThrows<IllegalArgumentException> { Employee.create(4) }
        assertEquals("Unknown career code: 4", exception.message)
    }
}
