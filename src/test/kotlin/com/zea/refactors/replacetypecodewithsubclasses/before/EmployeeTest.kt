package com.zea.refactors.replacetypecodewithsubclasses.before

import com.zea.refactors.replacetypecodewithsubclasses.before.Employee.Companion.ENGINEER
import com.zea.refactors.replacetypecodewithsubclasses.before.Employee.Companion.MANAGER
import com.zea.refactors.replacetypecodewithsubclasses.before.Employee.Companion.SALESMAN
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `creates an engineer employee`() {
        val employee = Employee(ENGINEER)
        assertEquals(ENGINEER, employee.profession)
    }

    @Test
    fun `creates an salesman employee`() {
        val employee = Employee(SALESMAN)
        assertEquals(SALESMAN, employee.profession)
    }

    @Test
    fun `creates an manager employee`() {
        val employee = Employee(MANAGER)
        assertEquals(MANAGER, employee.profession)
    }
}
