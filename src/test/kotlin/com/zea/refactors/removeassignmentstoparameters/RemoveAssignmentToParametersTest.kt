package com.zea.refactors.removeassignmentstoparameters

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RemoveAssignmentToParametersTest {

    @Test
    fun `Before Refactor`() {
        val discountService = BeforeRemoveAssignmentToParameters()
        assertEquals("Customer discount € 0.00",
            discountService.calculateDiscountFor(0, 70, 2))

        assertEquals("Customer discount € 100.00",
            discountService.calculateDiscountFor(55, 200, 10))
    }

    @Test
    fun `After Refactor - Remove Assignment to Parameters (Pass by reference in java)`() {
        val discountService = AfterRemoveAssignmentToParameters()
        assertEquals("Customer discount € 0.00",
            discountService.calculateDiscountFor(0, 70, 2))

        assertEquals("Customer discount € 100.00",
            discountService.calculateDiscountFor(55, 200, 10))
    }
}
