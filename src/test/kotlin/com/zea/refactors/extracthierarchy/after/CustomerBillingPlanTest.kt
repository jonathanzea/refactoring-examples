package com.zea.refactors.extracthierarchy.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CustomerBillingPlanTest {

    @Test
    fun `calculates billing for normal customer`() {
        val customer = Customer("Marivi", false)
        assertEquals("Customer: Marivi - Billing Plan: $100.0/month", customer.printBillingPlan())
    }

    @Test
    fun `calculates billing for disabled customer`() {
        val disabledCustomer = Customer("Angel", true)
        assertEquals("Customer: Angel - Billing Plan: $50.0/month", disabledCustomer.printBillingPlan())
    }
}
