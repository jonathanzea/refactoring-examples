package com.zea.refactors.extractinterface.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ItemCalculatorTest {

    @Test
    fun `calculates item for regular employee`() {
        val employee = RegularEmployee()
        val itemCalculator = ItemCalculator("Chocolate", 200.0)
        assertEquals(100.0, itemCalculator.calculateForRegularEmployee(employee))
    }

    @Test
    fun `calculates item for special needs employee`() {
        val employee = SpecialNeedsEmployee()
        val itemCalculator = ItemCalculator("Milk", 100.0)
        assertEquals(75.0, itemCalculator.calculateForSpecialNeedsEmployee(employee))
    }
}
