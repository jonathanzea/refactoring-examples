package com.zea.refactors.replacedatavaluewithobject.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class OrderTest {

    @Test
    fun `prints the order summary`() {
        val order = Order(110.0, 14, "Zsofi Jakabfi")
        assertEquals(order.print(), "ZSOFI - (14) items - Total: €110.0")
    }
}
