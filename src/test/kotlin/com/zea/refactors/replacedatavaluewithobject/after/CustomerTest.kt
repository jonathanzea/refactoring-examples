package com.zea.refactors.replacedatavaluewithobject.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CustomerTest {

    @Test
    fun `uppercases the first name`() {
        val customer = Customer("Maria Virgina Fereda")
        assertEquals("MARIA", customer.getFirstNameUppercased())
    }
}
