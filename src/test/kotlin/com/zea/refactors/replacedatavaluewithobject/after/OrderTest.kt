package com.zea.refactors.replacedatavaluewithobject.after

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class OrderTest {

    @Test
    fun `prints the order summary`() {
        val order = Order(110.0, 14, Customer("Zsofi Jakabfi"))
        Assertions.assertEquals(order.print(), "ZSOFI - (14) items - Total: €110.0")
    }
}
