package com.zea.refactors.encapsulatecollection.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StudentCourseTest {

    @Test
    fun `course list can be modified by adding more courses`() {
        val student = Student("Marivi")
        student.courses.add(Course("Cooking - French dishes", 140.50))
        assertEquals(1, student.courses.size)
    }

    @Test
    fun `course list can be replaced by another one`() {
        val student = Student("Michael")
        val courses = mutableListOf(
            Course("Sewing basics", 180.20),
            Course("Quantum Mechanics", 180.20),
            Course("Jewelry - Gold", 180.20)
        )
        student.courses = courses
        assertEquals(3, student.courses.size)
    }

    @Test
    fun `course list can be modified by reference`() {
        val student = Student("Marivi")
        val courses = mutableListOf(
            Course("Sewing basics", 180.20)
        )
        student.courses = courses
        assertEquals(1, student.courses.size)

        courses.add(Course("Driving Trucks", 320.50))

        assertEquals(2, student.courses.size)
    }

    @Test
    fun `course can be removed`() {
        val student = Student("Marivi")
        student.courses.add(Course("Cooking - French dishes", 140.50))
        assertEquals(1, student.courses.size)

        student.courses.removeAt(0)
        assertEquals(0, student.courses.size)
    }
}
