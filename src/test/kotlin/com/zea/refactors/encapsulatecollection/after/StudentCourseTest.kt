package com.zea.refactors.encapsulatecollection.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StudentCourseTest {

    @Test
    fun `a course can be added and the list of courses retrieved`() {
        val student = Student("Marivi")
        student.addCourse(Course("Navigation 1", 230.40))
        student.addCourse(Course("Navigation 2", 400.40))
        assertEquals(2, student.getCourses().size)
    }

    @Test
    fun `a list of courses can be added`() {
        val student = Student("Marivi")
        student.addCourse(Course("Navigation 1", 230.40))

        val courses = listOf(
            Course("Crafting wood", 100.00),
            Course("Swimming advanced", 300.45)
        )

        student.addAllCourses(courses)
        assertEquals(3, student.getCourses().size)
    }

    @Test
    fun `a course can be deleted by its name`() {
        val student = Student("Marivi")
        student.addCourse(Course("Navigation 1", 230.40))
        assertEquals(1, student.getCourses().size)

        student.removeCourse("Navigation 1")
        assertEquals(0, student.getCourses().size)
    }
}
