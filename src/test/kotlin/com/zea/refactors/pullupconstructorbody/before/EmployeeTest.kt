package com.zea.refactors.pullupconstructorbody.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `prints a major manager summary`() {
        val manager = Manager("Jack Kulen", 112, "MAJOR")
        assertEquals(
            "Name: Jack Kulen - Id: 112 - Grade: MAJOR - Vehicle: Cherokee Classic 1991",
            manager.printSummary()
        )
    }

    @Test
    fun `prints a manager summary`() {
        val manager = Manager("Jack Kulen", 1002, "REGULAR")
        assertEquals(
            "Name: Jack Kulen - Id: 1002 - Grade: REGULAR - Vehicle: none",
            manager.printSummary()
        )
    }

    @Test
    fun `prints a high salesman summary`() {
        val salesman = Salesman("Jack Kulen", 92928, 0.8)
        assertEquals(
            "Name: Jack Kulen - Id: 92928 - Commission: 0.8 - Vehicle: VW 1961",
            salesman.printSummary()
        )
    }

    @Test
    fun `prints a salesman summary`() {
        val salesman = Salesman("Jack Kulen", 1222, 0.3)
        assertEquals(
            "Name: Jack Kulen - Id: 1222 - Commission: 0.3 - Vehicle: none",
            salesman.printSummary()
        )
    }
}
