package com.zea.refactors.replacesubclasswithfields.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PersonTest {

    @Test
    fun `construct a subclass of a male peron`() {
        val male = Male()
        assertEquals('M', male.getCode())
        assertEquals(true, male.isMale())
    }

    @Test
    fun `construct a subclass of a female person`() {
        val female = Female()
        assertEquals('F', female.getCode())
        assertEquals(false, female.isMale())
    }
}
