package com.zea.refactors.replacesubclasswithfields.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PersonTest {

    @Test
    fun `construct a subclass of a male peron`() {
        val male = Person.createMale()
        assertEquals('M', male.code)
        assertEquals(true, male.isMale)
    }

    @Test
    fun `construct a subclass of a female person`() {
        val female = Person.createFemale()
        assertEquals('F', female.code)
        assertEquals(false, female.isMale)
    }
}
