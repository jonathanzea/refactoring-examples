package com.zea.refactors.introduceparameterobject.after

import java.time.LocalDate
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RoomPlannerTest {

    @Test
    fun `counts reservation inside of given period`() {
        val firstReservation = Reservation(LocalDate.of(2020, 9, 18))
        val secondReservation = Reservation(LocalDate.of(2020, 9, 30))
        val thirdReservation = Reservation(LocalDate.of(2020, 10, 29))

        val planner = RoomPlanner(
            listOf(
                firstReservation,
                secondReservation,
                thirdReservation
            )
        )
        val range = DateRange(
            LocalDate.of(2020, 9, 18),
            LocalDate.of(2020, 10, 29)
        )
        assertEquals(planner.countReservationsInPeriod(range), 3)
    }

    @Test
    fun `does not count reservation outside of given period`() {
        val firstReservation = Reservation(LocalDate.of(2020, 12, 10))
        val secondReservation = Reservation(LocalDate.of(2020, 12, 15))
        val thirdReservation = Reservation(LocalDate.of(2020, 12, 24))

        val range = DateRange(
            LocalDate.of(2020, 5, 10),
            LocalDate.of(2020, 12, 9)
        )
        val planner = RoomPlanner(
            listOf(
                firstReservation,
                secondReservation,
                thirdReservation
            )
        )
        assertEquals(planner.countReservationsInPeriod(range), 0)
    }
}
