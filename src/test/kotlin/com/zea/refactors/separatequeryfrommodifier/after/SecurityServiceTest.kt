package com.zea.refactors.separatequeryfrommodifier.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class SecurityServiceTest {

    @Test
    fun `prints the miscreant type 1 in the report and triggers alarm`() {
        val securityService =
            SecurityService("Marivi", "Edda", "Genesis")
        securityService.checkSecurity()
        assertEquals("Miscreant Found: Marivi", securityService.alertReport)
        assertTrue(securityService.alarmTriggered)
    }

    @Test
    fun `prints the miscreant type 2 in the report and triggers alarm`() {
        val securityService = SecurityService("Edda", "Genesis")
        securityService.checkSecurity()
        assertEquals("Miscreant Found: Edda", securityService.alertReport)
        assertTrue(securityService.alarmTriggered)
    }

    @Test
    fun `prints no miscreant in the report and triggers alarm`() {
        val securityService = SecurityService("Genesis")
        securityService.checkSecurity()
        assertEquals("No Miscreant Found", securityService.alertReport)
        assertFalse(securityService.alarmTriggered)
    }
}
