package com.zea.refactors.replaceparameterwithmethod.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class OrderPricingTest {

    @Test
    fun `calculates price for discount level 1`() {
        val order = Order("Puma Evloution", 50, 43.0)
        assertEquals(order.calculate(), "Puma Evloution (50) - €107.5")
    }

    @Test
    fun `calculates price for discount level 2`() {
        val order = Order("Nike Air", 150, 67.5)
        assertEquals(order.calculate(), "Nike Air (150) - €101.25")
    }
}
