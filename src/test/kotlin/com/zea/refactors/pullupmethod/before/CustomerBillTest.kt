package com.zea.refactors.pullupmethod.before

import java.time.LocalDate
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CustomerBillTest {

    @Test
    fun `charge small amount for preferred customer`() {
        val lastBillDate = LocalDate.of(2020, 10, 10)
        val newBillDate = LocalDate.of(2020, 10, 15)
        val customer = PreferredCustomer(lastBillDate)

        customer.createBill(newBillDate)
        assertEquals(customer.bills(), mapOf(newBillDate to 70.0))
    }

    @Test
    fun `charge big amount for preferred customer`() {
        val lastBillDate = LocalDate.of(2020, 10, 10)
        val newBillDate = LocalDate.of(2020, 10, 30)
        val customer = PreferredCustomer(lastBillDate)

        customer.createBill(newBillDate)
        assertEquals(customer.bills(), mapOf(newBillDate to 100.0))
    }

    @Test
    fun `charge small amount for regular customer`() {
        val lastBillDate = LocalDate.of(2020, 10, 10)
        val newBillDate = LocalDate.of(2020, 10, 15)
        val customer = RegularCustomer(lastBillDate)

        customer.createBill(newBillDate)
        assertEquals(customer.bills(), mapOf(newBillDate to 100.0))
    }

    @Test
    fun `charge big amount for regular customer`() {
        val lastBillDate = LocalDate.of(2020, 10, 10)
        val newBillDate = LocalDate.of(2020, 10, 30)
        val customer = RegularCustomer(lastBillDate)

        customer.createBill(newBillDate)
        assertEquals(customer.bills(), mapOf(newBillDate to 200.0))
    }
}
