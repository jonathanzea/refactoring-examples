package com.zea.refactors.replaceconstructorwithfactorymethod.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GenderTest {

    @Test
    fun `male returns gender code`() {
        val person = Male()
        assertEquals(person.genderCode, "M")
    }

    @Test
    fun `female returns gender code`() {
        val person = Female()
        assertEquals(person.genderCode, "F")
    }
}
