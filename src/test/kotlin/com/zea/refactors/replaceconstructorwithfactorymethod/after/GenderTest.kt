package com.zea.refactors.replaceconstructorwithfactorymethod.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GenderTest {

    @Test
    fun `male returns gender code`() {
        val person = Male.createMale()
        assertEquals(person.genderCode, "M")
    }

    @Test
    fun `female returns gender code`() {
        val person = Female.createFemale()
        assertEquals(person.genderCode, "F")
    }
}
