package com.zea.refactors.removemiddleman.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FinancePlannerTest {

    @Test
    fun `exposes the manager on charge of its department`() {
        val financePlanner = FinancePlanner(
            "Jonathan",
            ITCompany("Engineer")
        )
        assertEquals(60000.0, financePlanner.company.yearlySalary)
    }
}
