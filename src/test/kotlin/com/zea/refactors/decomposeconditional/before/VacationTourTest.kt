package com.zea.refactors.decomposeconditional.before

import java.time.LocalDate
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class VacationTourTest {

    @Test
    fun `calculates price for winter rate`() {
        val vacations =
            VacationTour(LocalDate.of(2020, 12, 25), 5, "Nevada")
        assertEquals("Trip to: Nevada - Price: €50.0", vacations.printItinerary())
    }

    @Test
    fun `calculates price for summer rate`() {
        val vacations =
            VacationTour(LocalDate.of(2020, 8, 25), 5, "New Mexico")
        assertEquals("Trip to: New Mexico - Price: €100.0", vacations.printItinerary())
    }
}
