package com.zea.refactors.replacedelegationwithinheritance.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `prints employee summary`() {
        val employee =
            Employee("Jonathan Zea", "ENGINEER", "Taiwan")
        assertEquals("Employee: Jonathan Zea (ENGINEER) - TAIWAN", employee.getSummary())
    }
}
