package com.zea.refactors.preservewholeobject.after

import org.junit.jupiter.api.Test

class RoomTemperatureServiceTest {

    @Test
    fun `succeeds on range inside temperature plan`() {
        val temperatureRange = TemperatureRange(7, 10)
        val freezerPlan = HeatingPlan(temperatureRange)
        val room = Room(7, 10, freezerPlan)
        assert(room.isWithinPlanRange())
    }

    @Test
    fun `fails on range that exceeds the minimum temperature peek in plan`() {
        val temperatureRange = TemperatureRange(10, 15)
        val coldPlan = HeatingPlan(temperatureRange)
        val room = Room(9, 13, coldPlan)
        assert(!room.isWithinPlanRange())
    }

    @Test
    fun `fails on range that exceeds the maximum temperature peek in plan`() {
        val temperatureRange = TemperatureRange(15, 37)
        val regularPlan = HeatingPlan(temperatureRange)
        val room = Room(18, 39, regularPlan)
        assert(!room.isWithinPlanRange())
    }
}
