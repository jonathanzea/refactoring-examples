package com.zea.refactors.replacetempwithquery

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReplaceTempWithQueryTest {

    @Test
    fun `Before Refactor`() {
        val shoes = BeforeReplaceTempWithQuery(2, 250.0)
        assertEquals(490.0, shoes.calculatePrice())

        val pants = BeforeReplaceTempWithQuery(3, 400.0)
        assertEquals(1140.0, pants.calculatePrice())
    }

    @Test
    fun `Refactor After - Replace temporary variable (temp) with query`() {
        val shoes = AfterReplaceTempWithQuery(2, 250.0)
        assertEquals(490.0, shoes.calculatePrice())

        val pants = AfterReplaceTempWithQuery(3, 400.0)
        assertEquals(1140.0, pants.calculatePrice())
    }
}
