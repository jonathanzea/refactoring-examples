package com.zea.refactors.selfencapsulatedfield.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DNITest {

    @Test
    fun `creates a dni from ID and a cuil from the dni`() {
        val jonathansDNI = DNI("95.567.330")
        assertEquals("95.567.330", jonathansDNI.id)

        val jonathansCUILD = CUIL("95.567.330")
        assertEquals("20-95.567.330-2", jonathansCUILD.id)
    }
}
