package com.zea.refactors.selfencapsulatedfield.before

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DNITest {

    @Test
    fun `creates a dni from ID and a cuil from the dni`() {
        val jonathansDNI = DNI("95.567.330")
        Assertions.assertEquals("95.567.330", jonathansDNI.id)

        val jonathansCUILD = CUIL(DNI("95.567.330"))
        Assertions.assertEquals("20-95.567.330-2", jonathansCUILD.getCuil())
    }
}
