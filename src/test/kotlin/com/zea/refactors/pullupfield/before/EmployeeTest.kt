package com.zea.refactors.pullupfield.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeTest {

    @Test
    fun `print summary for salesman`() {
        val salesman = Salesman("Marivi Fereda")
        assertEquals("Salesman (Marivi Fereda)", salesman.printSummary())
    }

    @Test
    fun `print summary for engineer`() {
        val engineer = Engineer("Marivi Fereda")
        assertEquals("Engineer (Marivi Fereda) - Area: Computing", engineer.printSummary())
    }
}
