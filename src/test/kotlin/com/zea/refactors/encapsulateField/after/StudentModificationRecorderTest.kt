package com.zea.refactors.encapsulateField.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StudentModificationRecorderTest {

    @Test
    fun `records the actions through getters and setters`() {
        val tudent = Student("Barbara")
        assertEquals("Barbara", tudent.getName())
        assertEquals(1, tudent.howManyTimesWasGotten("name"))

        tudent.setName("Krisel")
        assertEquals("Krisel", tudent.getName())
        assertEquals(2, tudent.howManyTimesWasGotten("name"))
        assertEquals(1, tudent.howManyTimesWasSet("name"))
    }
}
