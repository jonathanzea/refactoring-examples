package com.zea.refactors.encapsulateField.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StudentModificationRecorderTest {

    @Test
    fun `records the actions through getters and setters`() {
        val student = Student("Barbara")
        assertEquals("Barbara", student.name)
    }
}
