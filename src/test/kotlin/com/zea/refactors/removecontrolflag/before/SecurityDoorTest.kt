package com.zea.refactors.removecontrolflag.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SecurityDoorTest {

    @Test
    fun `detects and alarms on blacklisted persons`() {
        val securityDoor = SecurityDoor()
        val listOfPeople = mutableListOf("Angel", "Mary", "John", "Don")
        securityDoor.passing(listOfPeople)
        assertEquals(2, securityDoor.countAlerts())
        assertEquals(securityDoor.lastAlarm(), "ALARM! Blacklisted detected: Don")
    }
}
