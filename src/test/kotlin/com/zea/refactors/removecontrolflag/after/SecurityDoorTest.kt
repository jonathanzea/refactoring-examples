package com.zea.refactors.removecontrolflag.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SecurityDoorTest {

    @Test
    fun `detects and alarms on blacklisted persons`() {
        val securityDoor = SecurityDoor()
        val listOfPeople = mutableListOf("Angel", "Mary", "John", "Don")
        securityDoor.checkSecurity(listOfPeople)
        assertEquals(1, securityDoor.countAlerts())
        assertEquals(securityDoor.lastAlarm(), "ALARM! Blacklisted detected: John")
    }
}
