package com.zea.refactors.replacemagicnumberwithsymbolicconstant.after

import com.zea.refactors.changeunidirectionalassociationtobidirectional.after.Customer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FixedDepositTest {

    @Test
    fun `can calculate the interests generated`() {
        val fixedDeposit = FixedDeposit(
            Customer("Chan Yeibu"),
            40000.0,
            60
        )
        assertEquals("Interests: €65.75", fixedDeposit.calculate())
    }
}
