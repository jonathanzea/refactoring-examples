package com.zea.refactors.extractsubclass.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RegularItemTest {

    @Test
    fun `price normal job item`() {
        val item = RegularItem(55, 2)

        assertEquals(55, item.getUnitPrice())
        assertEquals(110, item.getTotalPrice())
    }

    @Test
    fun `price job item for employee`() {
        val employee = Employee(45)
        val item = DiscountItem(55, 2, employee)

        assertEquals(45, item.getUnitPrice())
        assertEquals(110, item.getTotalPrice())
    }
}
