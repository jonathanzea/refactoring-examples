package com.zea.refactors.extractsubclass.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RegularItemTest {

    @Test
    fun `price job for employee discount`() {
        val employee = Employee(45)
        val item = JobItem(55, 2, true, employee)

        assertEquals(45, item.getUnitPrice())
        assertEquals(110, item.getTotalPrice())
    }

    @Test
    fun `price job for employee`() {
        val employee = Employee(45)
        val item = JobItem(55, 2, false, employee)

        assertEquals(55, item.getUnitPrice())
        assertEquals(110, item.getTotalPrice())
    }
}
