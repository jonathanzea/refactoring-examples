package com.zea.refactors.replacearraywithobject.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SportServiceTest {

    @Test
    fun `can be constructed with a array representation of a team`() {
        val sportService = SportService(arrayOf("Liverpool", 10, 6))
        assertEquals("Liverpool", sportService.team[0] as String)
        assertEquals(10, sportService.team[1] as Int)
        assertEquals(6, sportService.team[2] as Int)
    }
}
