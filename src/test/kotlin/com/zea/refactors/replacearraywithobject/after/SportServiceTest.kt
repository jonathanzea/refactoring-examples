package com.zea.refactors.replacearraywithobject.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SportServiceTest {

    @Test
    fun `can be constructed with a Team object`() {
        val sportService = SportService(SoccerTeam("Liverpool", 10, 7))
        assertEquals("Liverpool", sportService.teamName())
        assertEquals(10, sportService.teamWins())
        assertEquals(7, sportService.teamLoses())
    }
}
