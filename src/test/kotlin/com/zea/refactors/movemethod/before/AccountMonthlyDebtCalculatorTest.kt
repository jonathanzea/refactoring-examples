package com.zea.refactors.movemethod.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AccountMonthlyDebtCalculatorTest {

    @Test
    fun `basic account debt with more than 7 overdrawn days`() {
        val account = AccountMonthlyDebtCalculator(
            AccountType("BASIC"),
            15
        )
        assertEquals(account.calculate(), 30.75)
    }

    @Test
    fun `basic account debt with less than 7 overdrawn days`() {
        val account =
            AccountMonthlyDebtCalculator(AccountType("BASIC"), 5)
        assertEquals(account.calculate(), 13.25)
    }

    @Test
    fun `basic account debt with no overdrawn days`() {
        val account =
            AccountMonthlyDebtCalculator(AccountType("BASIC"), 0)
        assertEquals(account.calculate(), 4.5)
    }

    @Test
    fun `premium account debt with more than 7 overdrawn days`() {
        val account = AccountMonthlyDebtCalculator(
            AccountType("PREMIUM"),
            15
        )
        assertEquals(account.calculate(), 25.25)
    }

    @Test
    fun `premium account debt with less than 7 overdrawn days`() {
        val account = AccountMonthlyDebtCalculator(
            AccountType("PREMIUM"),
            5
        )
        assertEquals(account.calculate(), 12.5)
    }

    @Test
    fun `premium account debt with no overdrawn days`() {
        val account = AccountMonthlyDebtCalculator(
            AccountType("PREMIUM"),
            0
        )
        assertEquals(account.calculate(), 4.5)
    }
}
