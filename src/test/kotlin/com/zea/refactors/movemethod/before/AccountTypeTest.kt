package com.zea.refactors.movemethod.before

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AccountTypeTest {

    @Test
    fun `constructed with BASIC identifier doesnt make a premium account`() {
        val accountType = AccountType("BASIC")
        assertFalse(accountType.isPremium())
    }

    @Test
    fun `constructed with PREMIUM identifier makes a premium account`() {
        val accountType = AccountType("PREMIUM")
        assertTrue(accountType.isPremium())
    }
}
