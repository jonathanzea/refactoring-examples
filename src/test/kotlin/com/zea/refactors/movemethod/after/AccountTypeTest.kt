package com.zea.refactors.movemethod.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AccountTypeTest {

    @Test
    fun `basic account overdraft with more than 7 days`() {
        val overdraftCharge = AccountType("BASIC").overdraftCharge(10)
        assertEquals(17.5, overdraftCharge)
    }

    @Test
    fun `basic account overdraft with less than 7 days`() {
        val overdraftCharge = AccountType("BASIC").overdraftCharge(5)
        assertEquals(8.75, overdraftCharge)
    }

    @Test
    fun `premium account overdraft with more than 7 days`() {
        val overdraftCharge = AccountType("PREMIUM").overdraftCharge(10)
        assertEquals(16.5, overdraftCharge)
    }

    @Test
    fun `premium account overdraft with less than 7 days`() {
        val overdraftCharge = AccountType("PREMIUM").overdraftCharge(5)
        assertEquals(8.0, overdraftCharge)
    }
}
