package com.zea.refactors.replacenestedconditionalwithguardclauses.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SalaryTest {

    @Test
    fun `calculates tax deduction for small salary`() {
        val salary = Salary(30000.0)
        assertEquals(0.0, salary.calculateMonthlyTaxDeduction())
    }

    @Test
    fun `calculates tax deduction for medium salary`() {
        val salary = Salary(40000.0)
        assertEquals(12000.0, salary.calculateMonthlyTaxDeduction())
    }

    @Test
    fun `calculates tax deduction for big salary`() {
        val salary = Salary(65000.0)
        assertEquals(26000.0, salary.calculateMonthlyTaxDeduction())
    }
}
