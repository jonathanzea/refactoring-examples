package com.zea.refactors.exractmethod

import com.zea.refactors.extractmethod.AfterExtractMethodNoLocalVariables
import com.zea.refactors.extractmethod.AfterExtractMethodReassigningALocalVariable
import com.zea.refactors.extractmethod.AfterExtractMethodReassigningALocalVariableWithInitializedTemp
import com.zea.refactors.extractmethod.AfterExtractMethodUsingLocalVariables
import com.zea.refactors.extractmethod.BeforeExtractMethod
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ExtractMethodTest {
    private val output = ByteArrayOutputStream()

    @BeforeEach
    fun setUpStreams() {
        System.setOut(PrintStream(output))
    }

    @AfterEach
    fun restoreStreams() {
        System.setOut(System.out)
    }

    @Test
    fun `Before Refactor`() {
        BeforeExtractMethod("Ibra", listOf(104.03, 660.20)).printDebt()
        assertEquals(
            "************************\n" +
                    "DEBT CALCULATOR\n" +
                    "************************\n" +
                    "Name: Ibra\n" +
                    "Total debt: $764.23\n",
            output.toString()
        )
    }

    @Test
    fun `Refactor after - Using No Local variable`() {
        AfterExtractMethodNoLocalVariables("Ibra", listOf(104.03, 660.20)).printDebt()
        assertEquals(
            "************************\n" +
                    "DEBT CALCULATOR\n" +
                    "************************\n" +
                    "Name: Ibra\n" +
                    "Total debt: $764.23\n",
            output.toString()
        )
    }

    @Test
    fun `Refactor after - Using Local Variables`() {
        AfterExtractMethodUsingLocalVariables("Ibra", listOf(104.03, 660.20)).printDebt()
        assertEquals(
            "************************\n" +
                    "DEBT CALCULATOR\n" +
                    "************************\n" +
                    "Name: Ibra\n" +
                    "Total debt: $764.23\n",
            output.toString()
        )
    }

    @Test
    fun `Refactor after - Reassigning a Local Variable`() {
        AfterExtractMethodReassigningALocalVariable("Ibra", listOf(104.03, 660.20)).printDebt()
        assertEquals(
            "************************\n" +
                    "DEBT CALCULATOR\n" +
                    "************************\n" +
                    "Name: Ibra\n" +
                    "Total debt: $764.23\n",
            output.toString()
        )
    }

    @Test
    fun `Refactor after - Reassigning a Local Variable - Variation for previous value in Temp`() {
        AfterExtractMethodReassigningALocalVariableWithInitializedTemp("Ibra", listOf(104.03, 660.20))
            .printDebt(150.45)
        assertEquals(
            "************************\n" +
                    "DEBT CALCULATOR\n" +
                    "************************\n" +
                    "Name: Ibra\n" +
                    "Total debt: $989.905\n",
            output.toString()
        )
    }
}
