package com.zea.refactors.substitutealgorithm

import com.zea.refactors.removeassignmentstoparameters.BeforeSubstituteAlgorithm
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BeforeSubstituteAlgorithmTest {

    @Test
    fun `Before Refactor`() {
        val personFinder = BeforeSubstituteAlgorithm("Larry", "Lenny", "Barry", "Moe", "Mickey")
        assertEquals("FOUND: Persons[0]=Larry", personFinder.find("Larry"))
        assertEquals("NOT FOUND", personFinder.find("Illinois"))
    }

    @Test
    fun `After Refactor - Substitute Algorithm`() {
        val personFinder = AfterSubstituteAlgorithm("Larry", "Lenny", "Barry", "Moe", "Mickey")
        assertEquals("FOUND: Persons[2]=Barry", personFinder.find("Barry"))
        assertEquals("NOT FOUND", personFinder.find("Velkiades"))
    }
}
