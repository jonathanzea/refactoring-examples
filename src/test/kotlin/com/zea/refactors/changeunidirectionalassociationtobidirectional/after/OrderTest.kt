package com.zea.refactors.changeunidirectionalassociationtobidirectional.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class OrderTest {

    @Test
    fun `can be created with a customer`() {
        val customer = Customer("John")
        val order = Order("Playstation 5", customer)

        assertEquals(Customer("John"), order.customer)
        assertEquals(setOf(Order("Playstation 5", customer)), customer.orders)
    }

    @Test
    fun `order and customer have bidirectional association`() {
        val customer = Customer("John")
        Order("Playstation 5", customer)
        Order("Nike Shoes", customer)

        assertEquals(
            setOf(
                Order("Playstation 5", customer),
                Order("Nike Shoes", customer)
            ), customer.orders
        )
        assertEquals(2, customer.orders.size)
    }

    @Test
    fun `customer reference can be modified and the reference from customer side disappear`() {
        val customer = Customer("John")
        val order = Order("Playstation 5", customer)

        assertEquals(1, customer.orders.size)

        order.modifyCustomer(Customer("Mary"))

        assertEquals(Customer("Mary"), order.customer)
        assertEquals(0, customer.orders.size)
    }
}
