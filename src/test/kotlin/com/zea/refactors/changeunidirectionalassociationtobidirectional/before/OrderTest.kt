package com.zea.refactors.changeunidirectionalassociationtobidirectional.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class OrderTest {

    @Test
    fun `can be created with a customer`() {
        val customer = Customer("Black Johnson")
        val order = Order("Adidas Shirt", customer)
        assertEquals(Customer("Black Johnson"), order.customer)
    }
}
