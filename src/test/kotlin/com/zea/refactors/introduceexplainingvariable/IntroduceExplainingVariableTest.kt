package com.zea.refactors.introduceexplainingvariable

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class IntroduceExplainingVariableTest {

    @Test
    fun `Before Refactor`() {
        val smallOrder = BeforeIntroduceExplainingVariable(5, 55.0)
        assertEquals(302.5, smallOrder.calculatePrice())

        val bigOrder = BeforeIntroduceExplainingVariable(670, 2450.0)
        assertEquals(1639517.5, bigOrder.calculatePrice())
    }

    @Test
    fun `After Refactor - Introduce Explaining Variable`() {
        val smallOrder = AfterIntroduceExplainingVariable(5, 55.0)
        assertEquals(302.5, smallOrder.calculatePrice())

        val bigOrder = AfterIntroduceExplainingVariable(670, 2450.0)
        assertEquals(1639517.5, bigOrder.calculatePrice())
    }

    @Test
    fun `After Refactor - Introduce Explaining Variable With Extract Method`() {
        val smallOrder = AfterIntroduceExplainingVariableWithExtractMethod(5, 55.0)
        assertEquals(302.5, smallOrder.calculatePrice())

        val bigOrder = AfterIntroduceExplainingVariableWithExtractMethod(670, 2450.0)
        assertEquals(1639517.5, bigOrder.calculatePrice())
    }
}
