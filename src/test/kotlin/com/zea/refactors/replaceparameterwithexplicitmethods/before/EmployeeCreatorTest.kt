package com.zea.refactors.replaceparameterwithexplicitmethods.before

import com.zea.refactors.replaceparameterwithexplicitmethods.after.EmployeeType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeCreatorTest {

    @Test
    fun `can construct an engineer`() {
        val engineer = EmployeeCreator(EmployeeType.ENGINEER)
            .create()
        assertEquals("engineer", engineer.getType())
    }

    @Test
    fun `can construct a salesman`() {
        val engineer = EmployeeCreator(EmployeeType.SALESMAN)
            .create()
        assertEquals("salesman", engineer.getType())
    }

    @Test
    fun `can construct a manager`() {
        val engineer = EmployeeCreator(EmployeeType.MANAGER)
            .create()
        assertEquals("manager", engineer.getType())
    }
}
