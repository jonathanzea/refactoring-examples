package com.zea.refactors.replaceparameterwithexplicitmethods.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EmployeeCreatorTest {

    @Test
    fun `can construct an engineer`() {
        val engineer = EmployeeCreator().createEngineer()
        assertEquals("engineer", engineer.getType())
    }

    @Test
    fun `can construct a salesman`() {
        val engineer = EmployeeCreator().createSalesman()
        assertEquals("salesman", engineer.getType())
    }

    @Test
    fun `can construct a manager`() {
        val engineer = EmployeeCreator().createManager()
        assertEquals("manager", engineer.getType())
    }
}
