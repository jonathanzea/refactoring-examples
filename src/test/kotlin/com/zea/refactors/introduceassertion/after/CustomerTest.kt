package com.zea.refactors.introduceassertion.after

import java.lang.IllegalArgumentException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class CustomerTest {

    @Test
    fun `constructs a customer`() {
        Customer(21, "Motje Eybe")
    }

    @Test
    fun `fails constructing an underage customer`() {
        val exception = assertThrows<IllegalArgumentException> {
            Customer(12, "Marivi Fereda")
        }
        assertEquals("Customers cannot be underage", exception.message)
    }
}
