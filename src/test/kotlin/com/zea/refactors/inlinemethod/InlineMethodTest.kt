package com.zea.refactors.inlinemethod

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class InlineMethodTest {

    @Test
    fun `Before Refactor`() {
        var deliveryRater = BeforeInlineMethod(4)
        assertEquals(1, deliveryRater.rating())
        deliveryRater = BeforeInlineMethod(6)
        assertEquals(2, deliveryRater.rating())
    }

    @Test
    fun `Refactor after - Inline`() {
        var deliveryRater = AfterInlineMethod(4)
        assertEquals(1, deliveryRater.rating())
        deliveryRater = AfterInlineMethod(6)
        assertEquals(2, deliveryRater.rating())
    }

    @Test
    fun `Refactor after - Inline when a temp is declared with a single method call`() {
        var deliveryRater = AfterInlineMethodWithTemp(2)
        assertEquals(1, deliveryRater.rating())
        deliveryRater = AfterInlineMethodWithTemp(6)
        assertEquals(2, deliveryRater.rating())
    }
}
