package com.zea.refactors.observerpattern.basic

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NewsAgencyTest {

    @Test
    fun `observable`() {
        val newsAgency = NewsAgency()
        val CNN = NewsChannel("CNN")
        val CBS = NewsChannel("CBS")
        newsAgency.addChannel(CNN)
        newsAgency.addChannel(CBS)
        newsAgency.transmitNews("Weather forecast")

        assertEquals("Weather forecast", CNN.nowTransmitting)
        assertEquals("Weather forecast", CBS.nowTransmitting)
    }
}

class NewsAgency {

    private val channels: MutableList<NewsChannel> = mutableListOf()

    fun addChannel(channel: NewsChannel) {
        channels.add(channel)
    }

    fun transmitNews(news: String) {
        channels.forEach { it.nowTransmitting = news }
    }
}

class NewsChannel(name: String) {
    var nowTransmitting: String = ""
}
