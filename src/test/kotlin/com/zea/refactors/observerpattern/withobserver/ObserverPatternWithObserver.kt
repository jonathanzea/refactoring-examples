package com.zea.refactors.observerpattern.withobserver

import java.util.Observable
import java.util.Observer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NewsAgencyTest {

    @Test
    fun `updates all the subscribed news channels`() {
        val newsAgency = NewsAgency()
        val CNN = NewsChannel("CNN")
        val CBS = NewsChannel("CBS")
        newsAgency.addObserver(CNN)
        newsAgency.addObserver(CBS)
        newsAgency.transmitNews("Weather forecast")

        assertEquals("CNN: Weather forecast", CNN.getTransmission())
        assertEquals("CBS: Weather forecast", CBS.getTransmission())
    }
}

class NewsAgency : Observable() {

    fun transmitNews(news: String) {
        setChanged()
        notifyObservers(news)
    }
}

class NewsChannel(private val name: String) : Observer {

    private var nowTransmitting: String = ""

    override fun update(o: Observable?, arg: Any?) {
        nowTransmitting = "$name: ${arg as String}"
    }

    fun getTransmission() = nowTransmitting
}
