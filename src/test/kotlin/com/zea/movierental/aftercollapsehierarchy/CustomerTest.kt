package com.zea.movierental.aftercollapsehierarchy

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class CustomerTest {

    @Test
    fun `generates an ASCII statement`() {
        val genesis = Customer(
            "Genesis",
            listOf(Rental(Movie("The Judge", 0), 3))
        )
        assertEquals(
            "Rental Record for Genesis\n" +
                    "\tThe Judge\t3.5\n" +
                    "Amount owed is 3.5\n" +
                    "You earned 1 frequent renter points",
            genesis.asciiStatement()
        )
        println(genesis.asciiStatement())
    }

    @Test
    fun `generates an HTML statement`() {
        val genesis = Customer(
            "Genesis",
            listOf(Rental(Movie("The Judge", 0), 3))
        )
        assertEquals(
            "<H1>Rentals for <EM>Genesis</EM></H1><P>\n" +
                    "The Judge: 3.5<BR>\n" +
                    "<P>You owe <EM>3.5</EM><P>\n" +
                    "On this rental you earned <EM>1</EM> frequent renter points<P>",
            genesis.htmlStatement()
        )
        println(genesis.htmlStatement())
    }

    @Test
    fun `generates a full statement on various rentals`() {
        val hector = Customer(
            "Hector",
            listOf(
                Rental(Movie("Sponge Bob", 2), 4),
                Rental(Movie("Apollo 13", 0), 4),
                Rental(Movie("Dumb and Dumber", 0), 4),
                Rental(Movie("Interstellar", 1), 4)
            )
        )
        assertTrue(hector.asciiStatement().contains("Rental Record for Hector\n"))
        assertTrue(hector.asciiStatement().contains("\tSponge Bob\t3.0\n"))
        assertTrue(hector.asciiStatement().contains("\tApollo 13\t5.0\n"))
        assertTrue(hector.asciiStatement().contains("\tDumb and Dumber\t5.0\n"))
        assertTrue(hector.asciiStatement().contains("\tInterstellar\t12.0\n"))
        assertTrue(hector.asciiStatement().contains("Amount owed is 25.0\n"))
        assertTrue(hector.asciiStatement().contains("You earned 5 frequent renter points"))
    }
}
