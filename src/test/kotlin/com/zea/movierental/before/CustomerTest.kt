package com.zea.movierental.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class CustomerTest {

    @Test
    fun `rents a regular movie a get the correspondent price`() {
        val customerRentingRegularMovie = Customer(
            "Genesis",
            listOf(Rental(Movie("The Judge", 0), 3))
        )
        assertEquals(
            "Rental Record for Genesis\n" +
                    "\tThe Judge\t3.5\n" +
                    "Amount owed is 3.5\n" +
                    "You earned 1 frequent renter points",
            customerRentingRegularMovie.statement()
        )
    }

    @Test
    fun `rents a new movie a get the correspondent price`() {
        val customerRentingNewMovie = Customer(
            "Dave",
            listOf(Rental(Movie("Brave Heart", 1), 5))
        )
        assertEquals(
            "Rental Record for Dave\n" +
                    "\tBrave Heart\t15.0\n" +
                    "Amount owed is 15.0\n" +
                    "You earned 2 frequent renter points",
            customerRentingNewMovie.statement()
        )
    }

    @Test
    fun `rents a children movie a get the correspondent price`() {
        val customerRentingChildrenMovie = Customer(
            "Mary",
            listOf(Rental(Movie("Lion King", 2), 4))
        )
        assertEquals(
            "Rental Record for Mary\n" +
                    "\tLion King\t3.0\n" +
                    "Amount owed is 3.0\n" +
                    "You earned 1 frequent renter points",
            customerRentingChildrenMovie.statement()
        )
    }

    @Test
    fun `rents various movies`() {
        val hector = Customer(
            "Hector",
            listOf(
                Rental(Movie("Sponge Bob", 2), 4),
                Rental(Movie("Apollo 13", 0), 4),
                Rental(Movie("Dumb and Dumber", 0), 4),
                Rental(Movie("Interstellar", 1), 4)
            )
        )
        assertTrue(hector.statement().contains("Rental Record for Hector\n"))
        assertTrue(hector.statement().contains("\tSponge Bob\t3.0\n"))
        assertTrue(hector.statement().contains("\tApollo 13\t5.0\n"))
        assertTrue(hector.statement().contains("\tDumb and Dumber\t5.0\n"))
        assertTrue(hector.statement().contains("\tInterstellar\t12.0\n"))
        assertTrue(hector.statement().contains("Amount owed is 25.0\n"))
        assertTrue(hector.statement().contains("You earned 5 frequent renter points"))
    }
}
