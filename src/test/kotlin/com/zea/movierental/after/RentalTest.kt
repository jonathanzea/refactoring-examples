package com.zea.movierental.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RentalTest {

    @Test fun `children movie rental charge for more less 3 days`() {
        val rental = Rental(Movie("Lion King", 2), 3)
        assertEquals(1.5, rental.getCharge())
        assertEquals(1, rental.getFrequenterPoints())
    }

    @Test fun `children movie rental charge for more than 3 days`() {
        val rental = Rental(Movie("Lion King", 2), 5)
        assertEquals(4.5, rental.getCharge())
        assertEquals(1, rental.getFrequenterPoints())
    }

    @Test fun `regular movie rental for more than 2 days`() {
        val rental = Rental(Movie("Seven", 0), 5)
        assertEquals(6.5, rental.getCharge())
        assertEquals(1, rental.getFrequenterPoints())
    }

    @Test fun `regular movie rental for less than 2 days`() {
        val rental = Rental(Movie("Seven", 0), 1)
        assertEquals(2.0, rental.getCharge())
        assertEquals(1, rental.getFrequenterPoints())
    }

    @Test fun `new release movie rental for more than 1 day`() {
        val rental = Rental(Movie("John Wick 7", 1), 7)
        assertEquals(21.0, rental.getCharge())
        assertEquals(2, rental.getFrequenterPoints())
    }

    @Test fun `new release movie rental for less than 1 day`() {
        val rental = Rental(Movie("John Wick 7", 1), 1)
        assertEquals(3.0, rental.getCharge())
        assertEquals(1, rental.getFrequenterPoints())
    }
}
