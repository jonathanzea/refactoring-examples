package com.zea.movierental.after

import java.lang.IllegalArgumentException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

class MovieTest {

    @Test
    fun `can be created with existing codes`() {
        assertDoesNotThrow { Movie("Example", 0) }
        assertDoesNotThrow { Movie("Example", 1) }
        assertDoesNotThrow { Movie("Example", 2) }
    }

    @Test
    fun `cannot be created with non-existent code`() {
        val exception = assertThrows<IllegalArgumentException> { Movie("Example", 3) }
        assertEquals("Incorrect Price Code: 3", exception.message)
    }
}
