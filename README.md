# Refactoring: Improving the Design of Existing Code Examples :notebook_with_decorative_cover:	

In this small project we use `Gradle` and `Kotlin` to write the examples given in the _Refactoring_ book by 
_Martin Fowler, Kent Beck and others_. The package structure is divided by the names of the refactor practices. In each subpackage you will see a `before` and `after` section. The `before` package contains the original example code and the `after` package contains the refactored version of the code following the good design principles stated in the book.

# Gitlab CI

There's a simple CI file with a build and a test job that run under a jdk9 image. The pipeline also prints the code coverage for the gitlab badge purpose.

Enjoy,

_Jonathan Zea - December 2019 - Berlin_ :ocean:
